<?php

namespace App\Models;

use Encore\Admin\Auth\Database\HasPermissions;
use Encore\Admin\Auth\Database\Role;
use Encore\Admin\Traits\AdminBuilder;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Administrator.
 *
 * @property Role[] $roles
 */
class Message extends Model
{
    use Authenticatable, AdminBuilder, HasPermissions;

    protected $fillable = [
        'text',
    ];
}
