<?php

namespace App\Providers;

use App\Admin\Extensions\RedirectService;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro(
            'updated',
            function ($key) {
                $resourcesPath = RedirectService::resource(-1);

                return RedirectService::redirectAfterSaving($resourcesPath, $key);
            }
        );

        Response::macro(
            'saved',
            function ($key) {
                $resourcesPath = RedirectService::resource(0);

                return RedirectService::redirectAfterSaving($resourcesPath, $key);
            }
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }

}
