<?php

namespace App\Modules\Managers\Entities;

class Manager
{
    /**
     * @var string
     */
    private $username;
    /**
     * @var string
     */
    private $name;
    /**
     * @var int
     */
    private $id;
    /**
     * @var array
     */
    private $roles;
    /**
     * @var array
     */
    private $permissions;
    /**
     * @var string
     */
    private $password;

    /**
     * Manager constructor.
     *
     * @param int $id
     * @param string $username
     * @param string $name
     * @param array $roles
     * @param array $permissions
     * @param string $password
     */
    public function __construct(
        int $id,
        string $username,
        string $name,
        array $roles, //todo change to lists
        array $permissions,
        string $password
    ) {
        $this->username = $username;
        $this->name = $name;
        $this->id = $id;
        $this->roles = $roles;
        $this->permissions = $permissions;
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username)
    {
        $this->username = $username;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @param array $roles
     */
    public function setRoles(array $roles)
    {
        $this->roles = $roles;
    }

    /**
     * @return array
     */
    public function getPermissions(): array
    {
        return $this->permissions;
    }

    /**
     * @param array $permissions
     */
    public function setPermissions(array $permissions)
    {
        $this->permissions = $permissions;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password)
    {
        if ($password !== $this->password) {
            $this->password = bcrypt($password);
        }
    }
}