<?php

namespace App\Modules\Managers\Entities;

class ManagerDTO
{
    /**
     * @var string
     */
    private $username;
    /**
     * @var string
     */
    private $name;
    /**
     * @var array
     */
    private $roles;
    /**
     * @var array
     */
    private $permissions;
    /**
     * @var string
     */
    private $password;

    /**
     * Manager constructor.
     *
     * @param string $username
     * @param string $name
     * @param array $roles
     * @param array $permissions
     * @param string $password
     */
    public function __construct(
        string $username,
        string $name,
        array $roles,
        array $permissions,
        string $password
    ) {
        $this->username = $username;
        $this->name = $name;
        $this->roles = $roles;
        $this->permissions = $permissions;
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @return array
     */
    public function getPermissions(): array
    {
        return $this->permissions;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }
}