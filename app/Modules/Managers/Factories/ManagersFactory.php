<?php

namespace App\Modules\Managers\Factories;

use App\Models\Administrator;
use App\Modules\Managers\Entities\Manager;
use App\Modules\Managers\Entities\ManagerDTO;
use Illuminate\Http\Request;

class ManagersFactory
{
    /**
     * @param Administrator $manager
     *
     * @return Manager
     */
    public function makeFromEloquent(Administrator $manager): Manager
    {
        $manager = new Manager(
            $manager->id,
            $manager->username,
            $manager->name,
            $manager->roles()->pluck('id')->toArray(),
            $manager->permissions()->pluck('id')->toArray(),
            $manager->password
        );

        return $manager;
    }

    /**
     * @param Request $request
     *
     * @return ManagerDTO
     */
    public function makeDTOFromRequest(Request $request): ManagerDTO
    {
        $managerDTO = new ManagerDTO(
            $request->username,
            $request->name,
            array_filter($request->roles ?? []),
            array_filter($request->permissions ?? []),
            bcrypt($request->password)
        );

        return $managerDTO;
    }
}