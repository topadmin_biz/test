<?php

namespace App\Modules\Managers;

use App\Modules\Managers\Entities\Manager;
use App\Modules\Managers\Entities\ManagerDTO;

class ManagersService
{
    /**
     * @var ManagersRepositoryInterface
     */
    private $managersRepository;

    /**
     * ManagersService constructor.
     *
     * @param ManagersRepositoryInterface $managersRepository
     */
    public function __construct(ManagersRepositoryInterface $managersRepository)
    {
        $this->managersRepository = $managersRepository;
    }

    /**
     * @param Manager $manager
     */
    public function save(Manager $manager): void
    {
        $this->managersRepository->save($manager);
    }

    /**
     * @param ManagerDTO $managerDTO
     */
    public function create(ManagerDTO $managerDTO)
    {
        $this->managersRepository->store($managerDTO);
    }

    /**
     * @param int $id
     *
     * @return Manager
     */
    public function findById(int $id): Manager
    {
        $manager = $this->managersRepository->findById($id);

        return $manager;
    }
}