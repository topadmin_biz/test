<?php

namespace App\Modules\Managers;

use App\Models\Administrator;
use App\Modules\Managers\Entities\Manager;
use App\Modules\Managers\Entities\ManagerDTO;
use App\Modules\Managers\Exceptions\ManagerNotFoundException;
use App\Modules\Managers\Factories\ManagersFactory;

class ManagersEloquentRepository implements ManagersRepositoryInterface
{
    /**
     * @var ManagersFactory
     */
    private $managersFactory;

    /**
     * ManagersEloquentRepository constructor.
     *
     * @param ManagersFactory $managersFactory
     */
    public function __construct(ManagersFactory $managersFactory)
    {
        $this->managersFactory = $managersFactory;
    }

    /**
     * @param Manager $manager
     */
    public function save(Manager $manager): void
    {
        $managerEloquent = Administrator::find($manager->getId());
        $managerEloquent->update(
            [
                'username' => $manager->getUsername(),
                'name' => $manager->getName(),
                'password' => $manager->getPassword(),
            ]
        );

        $managerEloquent->roles()->sync($manager->getRoles());
        $managerEloquent->permissions()->sync($manager->getPermissions());
    }

    public function store(ManagerDTO $managerDTO): void
    {
        $managerEloquent = Administrator::create(
            [
                'name' => $managerDTO->getName(),
                'username' => $managerDTO->getUsername(),
                'password' => $managerDTO->getPassword(),
            ]
        );

        $managerEloquent->roles()->sync($managerDTO->getRoles());
        $managerEloquent->permissions()->sync($managerDTO->getPermissions());
    }

    public function findById(int $id): Manager
    {
        $managerEloquent = Administrator::find($id);
        if (!$managerEloquent) {
            throw new ManagerNotFoundException();
        }
        $manager = $this->managersFactory->makeFromEloquent($managerEloquent);

        return $manager;
    }

}