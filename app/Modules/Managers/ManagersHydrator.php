<?php

namespace App\Modules\Managers;

use App\Modules\Managers\Entities\Manager;
use Illuminate\Http\Request;

class ManagersHydrator
{
    public function fillFromRequest(Manager $manager, Request $request)
    {
        $manager->setName($request->name);
        $manager->setUsername($request->username);
        $manager->setRoles(array_filter($request->roles ?? []));
        $manager->setPermissions(array_filter($request->permissions ?? []));
        $manager->setPassword($request->password);

        return $manager;
    }
}