<?php

namespace App\Modules\Managers;

use App\Modules\Managers\Entities\Manager;
use App\Modules\Managers\Entities\ManagerDTO;

interface ManagersRepositoryInterface
{
    /**
     * @param Manager $manager
     */
    public function save(Manager $manager): void;

    /**
     * @param ManagerDTO $managerDTO
     */
    public function store(ManagerDTO $managerDTO): void;

    /**
     * @param int $id
     *
     * @return Manager
     */
    public function findById(int $id): Manager;
}