<?php

namespace App\Modules\Managers;

use App\Modules\Managers\Factories\ManagersFactory;
use Illuminate\Container\Container;
use Illuminate\Support\ServiceProvider;

class ManagersServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            ManagersRepositoryInterface::class,
            function (Container $app) {
                return new ManagersEloquentRepository(
                    $app->make(ManagersFactory::class)
                );
            }
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
