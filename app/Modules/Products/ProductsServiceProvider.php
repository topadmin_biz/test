<?php

namespace App\Modules\Products;

use App\Modules\Products\Factories\ProductsFactory;
use Illuminate\Container\Container;
use Illuminate\Support\ServiceProvider;

class ProductsServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            ProductsRepositoryInterface::class,
            function (Container $app) {
                return new ProductsEloquentRepository(
                    $app->make(ProductsFactory::class)
                );
            }
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
