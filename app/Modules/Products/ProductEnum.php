<?php

namespace App\Modules\Products;

class ProductEnum
{
    const STATUS_ENDED = 1;
    const STATUS_PRE_ORDER = 2;
    const STATUS_GONE = 3; //todo

    const STATUS_LIST = [
        self::STATUS_ENDED => 'Закончился',
        self::STATUS_PRE_ORDER => 'Предзаказ',
        self::STATUS_GONE => '"Ушла" благотв. камера',
    ];

    const STATUSES = [
        self::STATUS_ENDED,
        self::STATUS_PRE_ORDER,
        self::STATUS_GONE,
    ];
}