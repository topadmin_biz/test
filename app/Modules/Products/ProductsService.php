<?php

namespace App\Modules\Products;

use App\Modules\Products\Entities\Product;
use App\Modules\Products\Entities\ProductDTO;

class ProductsService
{
    /**
     * @var ProductsRepositoryInterface
     */
    private $categoriesRepository;

    /**
     * ProductsService constructor.
     *
     * @param ProductsRepositoryInterface $categoriesRepository
     */
    public function __construct(ProductsRepositoryInterface $categoriesRepository)
    {
        $this->categoriesRepository = $categoriesRepository;
    }

    /**
     * @param Product $category
     */
    public function save(Product $category): void
    {
        $this->categoriesRepository->save($category);
    }

    /**
     * @param ProductDTO $categoryDTO
     */
    public function create(ProductDTO $categoryDTO)
    {
        $this->categoriesRepository->store($categoryDTO);
    }

    /**
     * @param int $id
     *
     * @return Product
     */
    public function findById(int $id): Product
    {
        $Product = $this->categoriesRepository->findById($id);

        return $Product;
    }
}