<?php

namespace App\Modules\Products;

use App\Modules\Products\Entities\Product;
use App\Modules\Products\Entities\ProductDTO;

interface ProductsRepositoryInterface
{
    /**
     * @param Product $category
     */
    public function save(Product $category): void;

    /**
     * @param ProductDTO $categoryDTO
     */
    public function store(ProductDTO $categoryDTO): void;

    /**
     * @param int $id
     *
     * @return Product
     */
    public function findById(int $id): Product;
}