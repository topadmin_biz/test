<?php

namespace App\Modules\Products\Entities;

use App\Modules\Common\Money\Money;

class ProductDTO
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var int
     */
    private $workaboxId;
    /**
     * @var string
     */
    private $linkToReview;
    /**
     * @var Money
     */
    private $price;
    /**
     * @var int
     */
    private $statusIfEnded;

    /**
     * ProductDTO constructor.
     *
     * @param string $name
     * @param int $workaboxId
     * @param string $linkToReview
     * @param int $statusIfEnded
     * @param Money $price
     */
    public function __construct(
        string $name,
        int $workaboxId,
        string $linkToReview,
        int $statusIfEnded,
        Money $price
    ) {
        $this->name = $name;
        $this->workaboxId = $workaboxId;
        $this->linkToReview = $linkToReview;
        $this->price = $price;
        $this->statusIfEnded = $statusIfEnded;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getWorkaboxId(): int
    {
        return $this->workaboxId;
    }

    /**
     * @return string
     */
    public function getLinkToReview(): string
    {
        return $this->linkToReview;
    }

    /**
     * @return Money
     */
    public function getPrice(): Money
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getStatusIfEnded(): int
    {
        return $this->statusIfEnded;
    }
}