<?php

namespace App\Modules\Products\Entities;

use App\Modules\Common\Money\Money;

class Product
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $workaboxId;
    /**
     * @var string
     */
    private $linkToReview;
    /**
     * @var Money
     */
    private $price;
    /**
     * @var int
     */
    private $statusIfEnded;

    /**
     * Product constructor.
     *
     * @param int $id
     * @param string $name
     * @param int $workaboxId
     * @param string $linkToReview
     * @param int $statusIfEnded
     * @param Money $price
     */
    public function __construct(
        int $id,
        string $name,
        int $workaboxId,
        string $linkToReview,
        int $statusIfEnded,
        Money $price
    ) {
        $this->name = $name;
        $this->id = $id;
        $this->workaboxId = $workaboxId;
        $this->linkToReview = $linkToReview;
        $this->price = $price;
        $this->statusIfEnded = $statusIfEnded;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getWorkaboxId(): int
    {
        return $this->workaboxId;
    }

    /**
     * @param int $workaboxId
     */
    public function setWorkaboxId(int $workaboxId)
    {
        $this->workaboxId = $workaboxId;
    }

    /**
     * @return string
     */
    public function getLinkToReview(): string
    {
        return $this->linkToReview;
    }

    /**
     * @param string $linkToReview
     */
    public function setLinkToReview(string $linkToReview)
    {
        $this->linkToReview = $linkToReview;
    }

    /**
     * @return Money
     */
    public function getPrice(): Money
    {
        return $this->price;
    }

    /**
     * @param Money $price
     */
    public function setPrice(Money $price)
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getStatusIfEnded(): int
    {
        return $this->statusIfEnded;
    }

    /**
     * @param int $statusIfEnded
     */
    public function setStatusIfEnded(int $statusIfEnded)
    {
        $this->statusIfEnded = $statusIfEnded;
    }
}