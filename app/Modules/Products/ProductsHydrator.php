<?php

namespace App\Modules\Products;

use App\Modules\Common\Money\Money;
use App\Modules\Products\Entities\Product;
use Illuminate\Http\Request;

class ProductsHydrator
{
    public function fillFromRequest(Product $product, Request $request)
    {
        $product->setName($request->name);
        $product->setWorkaboxId($request->workabox_id);
        $product->setPrice(Money::makeFromCurrencyUnits($request->price));
        $product->setLinkToReview($request->link_to_review);
        $product->setStatusIfEnded($request->status_if_ended);

        return $product;
    }
}