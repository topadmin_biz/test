<?php

namespace App\Modules\Products\Factories;

use App\Modules\Common\Money\Money;
use App\Modules\Products\Entities\Product;
use App\Modules\Products\Entities\ProductDTO;
use Illuminate\Http\Request;
use App\Models\Product as ProductEloquent;

class ProductsFactory
{
    /**
     * @param ProductEloquent $productEloquent
     *
     * @return Product
     */
    public function makeFromEloquent(ProductEloquent $productEloquent): Product
    {
        $product = new Product(
            $productEloquent->id,
            $productEloquent->name,
            $productEloquent->workabox_id,
            $productEloquent->link_to_review,
            $productEloquent->status_if_ended,
            Money::makeFromHundredths($productEloquent->price)
        );

        return $product;
    }

    /**
     * @param Request $request
     *
     * @return ProductDTO
     */
    public function makeDTOFromRequest(Request $request): ProductDTO
    {
        $productDTO = new ProductDTO(
            $request->name,
            $request->workabox_id,
            $request->link_to_review,
            $request->status_if_ended,
            Money::makeFromCurrencyUnits($request->price)
        );

        return $productDTO;
    }
}