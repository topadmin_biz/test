<?php

namespace App\Modules\Products;

use App\Modules\Products\Entities\Product;
use App\Modules\Products\Entities\ProductDTO;
use App\Modules\Products\Exceptions\ProductNotFoundException;
use App\Modules\Products\Factories\ProductsFactory;
use App\Models\Product as ProductEloquent;

class ProductsEloquentRepository implements ProductsRepositoryInterface
{
    /**
     * @var ProductsFactory
     */
    private $productsFactory;

    /**
     * ProductsEloquentRepository constructor.
     *
     * @param ProductsFactory $productsFactory
     */
    public function __construct(ProductsFactory $productsFactory)
    {
        $this->productsFactory = $productsFactory;
    }

    /**
     * @param Product $product
     */
    public function save(Product $product): void
    {
        $productEloquent = ProductEloquent::find($product->getId());
        $productEloquent->update(
            [
                'name' => $product->getName(),
                'link_to_review' => $product->getLinkToReview(),
                'price' => $product->getPrice()->getAmountInHundredths(),
                'status_if_ended' => $product->getStatusIfEnded(),
                'workabox_id' => $product->getWorkaboxId(),
            ]
        );
    }

    /**
     * @param ProductDTO $productDTO
     */
    public function store(ProductDTO $productDTO): void
    {
        ProductEloquent::create(
            [
                'name' => $productDTO->getName(),
                'link_to_review' => $productDTO->getLinkToReview(),
                'price' => $productDTO->getPrice()->getAmountInHundredths(),
                'status_if_ended' => $productDTO->getStatusIfEnded(),
                'workabox_id' => $productDTO->getWorkaboxId(),
            ]
        );
    }

    /**
     * @param int $id
     *
     * @return Product
     * @throws ProductNotFoundException
     */
    public function findById(int $id): Product
    {
        $productEloquent = ProductEloquent::find($id);
        if (!$productEloquent) {
            throw new ProductNotFoundException();
        }
        $product = $this->productsFactory->makeFromEloquent($productEloquent);

        return $product;
    }
}