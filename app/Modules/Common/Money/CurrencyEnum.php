<?php

namespace App\Modules\Common\Money;

class CurrencyEnum
{
    const USD = 'USD';

    const UAH = 'UAH';

    const DEFAULT_CURRENCY = self::UAH;

    const AVAILABLE_CURRENCY = [
        self::USD,
        self::UAH,
    ];
}