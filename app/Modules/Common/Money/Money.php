<?php

namespace App\Modules\Common\Money;

class Money
{
    /**
     * @var int
     */
    private $amountCent;
    /**
     * @var Currency
     */
    private $currency;

    /**
     * Money constructor.
     *
     * @param int $amountCent
     * @param Currency $currency
     */
    public function __construct(int $amountCent = 0, Currency $currency = null)
    {
        $this->amountCent = $amountCent;
        $this->currency = $currency ? $currency : new Currency();
    }

    /**
     * @return Currency
     */
    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    /**
     * @param Currency $currency
     */
    public function setCurrency(Currency $currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return (float)$this->amountCent / 100;
    }

    /**
     * @return int
     */
    public function getAmountInHundredths() : int
    {
        return $this->amountCent;
    }

    /**
     * @param int $amountCent
     */
    public function setAmount(int $amountCent): void
    {
        $this->amountCent = $amountCent;
    }

    /**
     * @param Money $money
     */
    public function increase(Money $money): void
    {
        $this->amountCent += $money->getAmountInHundredths();
    }

    /**
     * @param Money $money
     */
    public function decrease(Money $money): void
    {
        $this->amountCent -= $money->getAmountInHundredths();
    }

    public function biggerThan(Money $money): bool
    {
        return $this->amountCent > $money->getAmountInHundredths();
    }

    public function biggerOrEqual(Money $money): bool
    {
        return $this->amountCent >= $money->getAmountInHundredths();
    }

    public function isEqual(Money $money): bool
    {
        return $this->amountCent === $money->getAmountInHundredths();
    }

    public function isBiggerThanZero(): bool
    {
        return $this->amountCent > 0;
    }

    public function multiply(int $amount)
    {
        $this->amountCent *= $amount;
    }

    static function makeFromHundredths(int $cents): Money
    {
        return new Money($cents);
    }

    static function makeFromCurrencyUnits(float $currencyUnits): Money
    {
        return new Money(round($currencyUnits * 100));
    }

    static function make(): Money
    {
        return new Money();
    }
}