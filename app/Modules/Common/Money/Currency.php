<?php

namespace App\Modules\Common\Money;

class Currency
{
    /**
     * @var string
     */
    private $currency;

    /**
     * Currency constructor.
     *
     * @param string $currency
     *
     * @throws WrongCurrencyException
     */
    public function __construct(string $currency = CurrencyEnum::DEFAULT_CURRENCY)
    {
        $this->validate($currency);

        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getCurrencyName(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency)
    {
        $this->currency = $currency;
    }

    /**
     * @param string $currency
     *
     * @throws WrongCurrencyException
     */
    private function validate(string $currency)
    {
        if (!in_array($currency, CurrencyEnum::AVAILABLE_CURRENCY)) {
            throw new WrongCurrencyException();
        }
    }
}