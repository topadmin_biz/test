<?php
namespace App\Modules\Common\Money;

use Exception;

class WrongCurrencyException extends Exception
{
    protected $message = 'Wrong Currency Exception';
}