<?php

namespace App\Modules\Categories;

use App\Modules\Categories\Entities\Category;
use App\Modules\Categories\Entities\CategoryDTO;
use App\Modules\Categories\Exceptions\CategoryNotFoundException;
use App\Modules\Categories\Factories\CategoriesFactory;
use App\Models\Category as CategoryEloquent;

class CategoriesEloquentRepository implements CategoriesRepositoryInterface
{
    /**
     * @var CategoriesFactory
     */
    private $categoriesFactory;

    /**
     * CategoriesEloquentRepository constructor.
     *
     * @param CategoriesFactory $categoriesFactory
     */
    public function __construct(CategoriesFactory $categoriesFactory)
    {
        $this->categoriesFactory = $categoriesFactory;
    }

    /**
     * @param Category $category
     */
    public function save(Category $category): void
    {
        $categoryEloquent = CategoryEloquent::find($category->getId());
        $categoryEloquent->update(
            [
                'name' => $category->getName(),
            ]
        );
    }

    /**
     * @param CategoryDTO $categoryDTO
     */
    public function store(CategoryDTO $categoryDTO): void
    {
        CategoryEloquent::create(
            [
                'name' => $categoryDTO->getName(),
            ]
        );
    }

    /**
     * @param int $id
     *
     * @return Category
     * @throws CategoryNotFoundException
     */
    public function findById(int $id): Category
    {
        $categoryEloquent = CategoryEloquent::find($id);
        if (!$categoryEloquent) {
            throw new CategoryNotFoundException();
        }
        $category = $this->categoriesFactory->makeFromEloquent($categoryEloquent);

        return $category;
    }
}