<?php

namespace App\Modules\Categories;

use App\Modules\Categories\Entities\Category;
use Illuminate\Http\Request;

class CategoriesHydrator
{
    public function fillFromRequest(Category $category, Request $request)
    {
        $category->setName($request->name);

        return $category;
    }
}