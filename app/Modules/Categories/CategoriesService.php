<?php

namespace App\Modules\Categories;

use App\Modules\Categories\Entities\Category;
use App\Modules\Categories\Entities\CategoryDTO;

class CategoriesService
{
    /**
     * @var CategoriesRepositoryInterface
     */
    private $categoriesRepository;

    /**
     * CategoriesService constructor.
     *
     * @param CategoriesRepositoryInterface $categoriesRepository
     */
    public function __construct(CategoriesRepositoryInterface $categoriesRepository)
    {
        $this->categoriesRepository = $categoriesRepository;
    }

    /**
     * @param Category $category
     */
    public function save(Category $category): void
    {
        $this->categoriesRepository->save($category);
    }

    /**
     * @param CategoryDTO $categoryDTO
     */
    public function create(CategoryDTO $categoryDTO)
    {
        $this->categoriesRepository->store($categoryDTO);
    }

    /**
     * @param int $id
     *
     * @return Category
     */
    public function findById(int $id): Category
    {
        $Category = $this->categoriesRepository->findById($id);

        return $Category;
    }
}