<?php

namespace App\Modules\Categories\Entities;

class Category
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var int
     */
    private $id;

    /**
     * Category constructor.
     *
     * @param int $id
     * @param string $name
     */
    public function __construct(
        int $id,
        string $name
    ) {
        $this->name = $name;
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}