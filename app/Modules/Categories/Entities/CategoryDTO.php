<?php

namespace App\Modules\Categories\Entities;

class CategoryDTO
{
    /**
     * @var string
     */
    private $name;

    /**
     * CategoryDTO constructor.
     *
     * @param string $name
     */
    public function __construct(
        string $name
    ) {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}