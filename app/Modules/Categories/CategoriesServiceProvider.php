<?php

namespace App\Modules\Categories;

use App\Modules\Categories\Factories\CategoriesFactory;
use Illuminate\Container\Container;
use Illuminate\Support\ServiceProvider;

class CategoriesServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            CategoriesRepositoryInterface::class,
            function (Container $app) {
                return new CategoriesEloquentRepository(
                    $app->make(CategoriesFactory::class)
                );
            }
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
