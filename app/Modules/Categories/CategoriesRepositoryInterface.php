<?php

namespace App\Modules\Categories;

use App\Modules\Categories\Entities\Category;
use App\Modules\Categories\Entities\CategoryDTO;

interface CategoriesRepositoryInterface
{
    /**
     * @param Category $category
     */
    public function save(Category $category): void;

    /**
     * @param CategoryDTO $categoryDTO
     */
    public function store(CategoryDTO $categoryDTO): void;

    /**
     * @param int $id
     *
     * @return Category
     */
    public function findById(int $id): Category;
}