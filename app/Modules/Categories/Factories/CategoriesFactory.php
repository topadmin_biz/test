<?php

namespace App\Modules\Categories\Factories;

use App\Modules\Categories\Entities\Category;
use App\Modules\Categories\Entities\CategoryDTO;
use Illuminate\Http\Request;
use App\Models\Category as CategoryEloquent;

class CategoriesFactory
{
    /**
     * @param CategoryEloquent $category
     *
     * @return Category
     */
    public function makeFromEloquent(CategoryEloquent $category): Category
    {
        $category = new Category(
            $category->id,
            $category->name
        );

        return $category;
    }

    /**
     * @param Request $request
     *
     * @return CategoryDTO
     */
    public function makeDTOFromRequest(Request $request): CategoryDTO
    {
        $categoryDTO = new CategoryDTO(
            $request->name
        );

        return $categoryDTO;
    }
}