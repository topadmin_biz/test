<?php

namespace App\Modules\Cities;

use App\Modules\Cities\Entities\City;
use App\Modules\Cities\Entities\CityDTO;

class CitiesService
{
    /**
     * @var CitiesRepositoryInterface
     */
    private $citiesRepository;

    /**
     * CitiesService constructor.
     *
     * @param CitiesRepositoryInterface $citiesRepository
     */
    public function __construct(CitiesRepositoryInterface $citiesRepository)
    {
        $this->citiesRepository = $citiesRepository;
    }

    /**
     * @param City $city
     */
    public function save(City $city): void
    {
        $this->citiesRepository->save($city);
    }

    /**
     * @param CityDTO $cityDTO
     */
    public function create(CityDTO $cityDTO)
    {
        $this->citiesRepository->store($cityDTO);
    }

    /**
     * @param int $id
     *
     * @return City
     */
    public function findById(int $id): City
    {
        $city = $this->citiesRepository->findById($id);

        return $city;
    }
}