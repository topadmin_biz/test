<?php

namespace App\Modules\Cities;

use App\Modules\Cities\Entities\City;
use App\Modules\Cities\Entities\CityDTO;

interface CitiesRepositoryInterface
{
    /**
     * @param City $city
     */
    public function save(City $city): void;

    /**
     * @param CityDTO $cityDTO
     */
    public function store(CityDTO $cityDTO): void;

    /**
     * @param int $id
     *
     * @return City
     */
    public function findById(int $id): City;
}