<?php

namespace App\Modules\Cities;

use App\Modules\Cities\Entities\City;
use App\Modules\Cities\Entities\CityDTO;
use App\Modules\Cities\Exceptions\CityNotFoundException;
use App\Modules\Cities\Factories\CitiesFactory;
use App\Models\City as CityEloquent;

class CitiesEloquentRepository implements CitiesRepositoryInterface
{
    /**
     * @var CitiesFactory
     */
    private $citiesFactory;

    /**
     * CitiesEloquentRepository constructor.
     *
     * @param CitiesFactory $citiesFactory
     */
    public function __construct(CitiesFactory $citiesFactory)
    {
        $this->citiesFactory = $citiesFactory;
    }

    /**
     * @param City $city
     */
    public function save(City $city): void
    {
        $cityEloquent = CityEloquent::find($city->getId());
        $cityEloquent->update(
            [
                'name' => $city->getName(),
            ]
        );
    }

    public function store(CityDTO $cityDTO): void
    {
        CityEloquent::create(
            [
                'name' => $cityDTO->getName(),
            ]
        );
    }

    public function findById(int $id): City
    {
        $cityEloquent = CityEloquent::find($id);
        if (!$cityEloquent) {
            throw new CityNotFoundException();
        }
        $city = $this->citiesFactory->makeFromEloquent($cityEloquent);

        return $city;
    }

}