<?php

namespace App\Modules\Cities;

use App\Modules\Cities\Entities\City;
use Illuminate\Http\Request;

class CitiesHydrator
{
    public function fillFromRequest(City $city, Request $request)
    {
        $city->setName($request->name);

        return $city;
    }
}