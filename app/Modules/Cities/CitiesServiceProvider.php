<?php

namespace App\Modules\Cities;

use App\Modules\Cities\Factories\CitiesFactory;
use Illuminate\Container\Container;
use Illuminate\Support\ServiceProvider;

class CitiesServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            CitiesRepositoryInterface::class,
            function (Container $app) {
                return new CitiesEloquentRepository(
                    $app->make(CitiesFactory::class)
                );
            }
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
