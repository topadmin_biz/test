<?php

namespace App\Modules\Cities\Factories;

use App\Modules\Cities\Entities\City;
use App\Modules\Cities\Entities\CityDTO;
use Illuminate\Http\Request;
use App\Models\City as CityEloquent;

class CitiesFactory
{
    /**
     * @param CityEloquent $city
     *
     * @return City
     */
    public function makeFromEloquent(CityEloquent $city): City
    {
        $city = new City(
            $city->id,
            $city->name
        );

        return $city;
    }

    /**
     * @param Request $request
     *
     * @return CityDTO
     */
    public function makeDTOFromRequest(Request $request): CityDTO
    {
        $cityDTO = new CityDTO(
            $request->name
        );

        return $cityDTO;
    }
}