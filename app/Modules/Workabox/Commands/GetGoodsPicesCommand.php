<?php

namespace App\Modules\Workabox\Commands;

use Workabox\API\ObjectID;

class GetGoodsPricesCommand extends WorkaboxCommand
{
    public function execute($storeID = 1004): array //todo delete magic number 1004 - kiev
    {
        $store = new ObjectID();

        $store->ID = $storeID;

        $list = $this->workaboxClient->getGoodsPrices($store);

        return $list;
    }
}