<?php

namespace App\Modules\Workabox\Commands;

use Workabox\API\CustomerNaturalPersonFilter;

class GetCustomerCommand extends WorkaboxCommand
{
    public function execute($customerId): array
    {
        $filter = new CustomerNaturalPersonFilter(["ID" => $customerId]);

        $customer = $this->workaboxClient->getCustomer($filter);

        return $customer;
    }
}