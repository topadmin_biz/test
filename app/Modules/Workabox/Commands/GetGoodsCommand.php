<?php

namespace App\Modules\Workabox\Commands;

use Workabox\API\GoodsFilter;

class GetGoodsCommand extends WorkaboxCommand
{
    public function execute($productID = null): array
    {
        if ($productID) {
            $filter = new GoodsFilter();
        } else {
            $filter = new GoodsFilter(["ID" => $productID]);
        }
        $list = $this->workaboxClient->getGoods($filter);

        return $list;
    }
}