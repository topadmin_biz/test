<?php

namespace App\Modules\Workabox\Commands;

use App\Modules\Workabox\WorkaboxClient;

abstract class WorkaboxCommand
{
    /**
     * @var WorkaboxClient
     */
    protected $workaboxClient;

    /**
     * WorkaboxCommand constructor.
     *
     * @param WorkaboxClient $workaboxClient
     */
    public function __construct(WorkaboxClient $workaboxClient)
    {
        $this->workaboxClient = $workaboxClient;
    }
}