<?php

namespace App\Modules\Workabox\Commands;

use Workabox\API\RetailSaleFilter;

class GetRetailSaleCommand extends WorkaboxCommand
{
    public function execute($customerId): array
    {
        $filter = new RetailSaleFilter(["Client" => $customerId]);

        $customer = $this->workaboxClient->getRetailSale($filter);

        return $customer;
    }
}