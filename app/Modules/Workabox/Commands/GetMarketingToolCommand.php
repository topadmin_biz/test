<?php

namespace App\Modules\Workabox\Commands;

use Workabox\API\ActualizingMIFilter;

class GetMarketingInstrumentCommand extends WorkaboxCommand
{
    public function execute($marketingInstrumentID): array
    {
        $filter = new ActualizingMIFilter(["ID" => $marketingInstrumentID]);

        $list = $this->workaboxClient->getMarketingInstrument($filter);

        return $list;
    }
}