<?php

namespace App\Modules\Workabox;

use Thrift\Protocol\TBinaryProtocol;
use Thrift\Transport\THttpClient;
use Workabox\API\APIServiceClient;

class WorkaboxClient
{
    private $client;
    private $transport;
    private $protocol;

    public function __construct()
    {
        $this->login = config('services.workabox.login');
        $this->password = config('services.workabox.password');
        $workaboxEndpoint = config('services.workabox.endpoint');
        $workaboxPort = config('services.workabox.port');
        $this->transport = new THttpClient(
            $workaboxEndpoint, $workaboxPort, 'API.Binary.thrift'
        );
        $this->protocol = new TBinaryProtocol($this->transport);
        $this->client = new APIServiceClient($this->protocol);
    }

    public function open(): void
    {
        $this->transport->open();
    }

    public function close(): void
    {
        $this->transport->close();
    }

    /**
     * @return string
     */
    public function getLoginKey(): string
    {
        $key = $this->client->Login($this->login, $this->password);

        return $key;
    }

    public function getGoods($filter): array //todo change to list
    {
        $this->open();
        $key = $this->getLoginKey();
        $list = $this->client->ExportGoods($key, null, $filter);
        $this->close();

        return $list;
    }

    public function getGoodsQuantity($store): array //todo change to list
    {
        $this->open();
        $key = $this->getLoginKey();
        $list = $this->client->ExportGoodsQuantity($key, $store);
        $this->close();

        return $list;
    }

    public function getGoodsPrices($store): array //todo change to list
    {
        $this->open();
        $key = $this->getLoginKey();
        $list = $this->client->ExportPriceListRTT($key, $store);
        $this->close();

        return $list;
    }

    public function getCustomer($filter): array //todo change to list
    {
        $this->open();
        $key = $this->getLoginKey();
        $list = $this->client->ExportCustomerNaturalPerson($key, null, $filter);
        $this->close();

        return $list;
    }

    public function getRetailSale($filter): array //todo change to list
    {
        $this->open();
        $key = $this->getLoginKey();
        $list = $this->client->ExportRetailSale($key, null, $filter);
        $this->close();

        return $list;
    }

    public function getMarketingInstrument($filter)
    {
        $this->open();
        $key = $this->getLoginKey();
        $list = $this->client->ExportActualizingMI($key, null, $filter);
        $this->close();

        return $list;
    }
}