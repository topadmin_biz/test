<?php

namespace App\Modules\Messages;

use App\Modules\Messages\Entities\Message;
use App\Modules\Messages\Entities\MessageDTO;

interface MessagesRepositoryInterface
{
    /**
     * @param Message $message
     */
    public function save(Message $message): void;

    /**
     * @param MessageDTO $messageDTO
     */
    public function store(MessageDTO $messageDTO): void;

    /**
     * @param int $id
     *
     * @return Message
     */
    public function findById(int $id): Message;
}