<?php

namespace App\Modules\Messages;

use App\Modules\Messages\Factories\MessagesFactory;
use Illuminate\Container\Container;
use Illuminate\Support\ServiceProvider;

class MessagesServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            MessagesRepositoryInterface::class,
            function (Container $app) {
                return new MessagesEloquentRepository(
                    $app->make(MessagesFactory::class)
                );
            }
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
