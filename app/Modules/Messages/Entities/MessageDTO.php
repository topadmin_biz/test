<?php

namespace App\Modules\Messages\Entities;

class MessageDTO
{
    /**
     * @var string
     */
    private $text;

    /**
     * MessageDTO constructor.
     *
     * @param string $text
     */
    public function __construct(string $text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }
}