<?php

namespace App\Modules\Messages\Entities;

class Message
{
    /**
     * @var string
     */
    private $text;
    /**
     * @var int
     */
    private $id;

    /**
     * Message constructor.
     *
     * @param int $id
     * @param string $text
     */
    public function __construct(int $id, string $text)
    {
        $this->text = $text;
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}