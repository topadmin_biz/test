<?php

namespace App\Modules\Messages\Factories;

use App\Models\Message as MessageEloquent;
use App\Modules\Messages\Entities\Message;
use App\Modules\Messages\Entities\MessageDTO;
use Illuminate\Http\Request;

class MessagesFactory
{
    /**
     * @param MessageEloquent $message
     *
     * @return Message
     */
    public function makeFromEloquent(MessageEloquent $message): Message
    {
        $message = new Message(
            $message->id,
            $message->text
        );

        return $message;
    }

    /**
     * @param Request $request
     *
     * @return MessageDTO
     */
    public function makeDTOFromRequest(Request $request): MessageDTO
    {
        $messageDTO = new MessageDTO(
            $request->text
        );

        return $messageDTO;
    }
}