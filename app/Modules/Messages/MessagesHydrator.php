<?php

namespace App\Modules\Messages;

use App\Modules\Messages\Entities\Message;
use Illuminate\Http\Request;

class MessagesHydrator
{
    public function fillFromRequest(Message $message, Request $request)
    {
        $message->setText($request->text);

        return $message;
    }
}