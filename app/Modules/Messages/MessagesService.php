<?php

namespace App\Modules\Messages;

use App\Modules\Messages\Entities\Message;
use App\Modules\Messages\Entities\MessageDTO;

class MessagesService
{
    /**
     * @var MessagesRepositoryInterface
     */
    private $messagesRepository;

    /**
     * MessagesService constructor.
     *
     * @param MessagesRepositoryInterface $messagesRepository
     */
    public function __construct(MessagesRepositoryInterface $messagesRepository)
    {
        $this->messagesRepository = $messagesRepository;
    }

    /**
     * @param Message $message
     */
    public function save(Message $message): void
    {
        $this->messagesRepository->save($message);
    }

    /**
     * @param MessageDTO $messageDTO
     */
    public function create(MessageDTO $messageDTO)
    {
        $this->messagesRepository->store($messageDTO);
    }

    /**
     * @param int $id
     *
     * @return Message
     */
    public function findById(int $id): Message
    {
        $message = $this->messagesRepository->findById($id);

        return $message;
    }
}