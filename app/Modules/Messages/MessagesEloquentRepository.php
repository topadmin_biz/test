<?php

namespace App\Modules\Messages;

use App\Modules\Messages\Entities\Message;
use App\Modules\Messages\Entities\MessageDTO;
use App\Modules\Messages\Exceptions\MessageNotFoundException;
use App\Modules\Messages\Factories\MessagesFactory;
use App\Models\Message as MessageEloquent;

class MessagesEloquentRepository implements MessagesRepositoryInterface
{
    /**
     * @var MessagesFactory
     */
    private $messagesFactory;

    /**
     * MessagesEloquentRepository constructor.
     *
     * @param MessagesFactory $messagesFactory
     */
    public function __construct(MessagesFactory $messagesFactory)
    {
        $this->messagesFactory = $messagesFactory;
    }

    /**
     * @param Message $message
     */
    public function save(Message $message): void
    {
        $messageEloquent = MessageEloquent::find($message->getId());
        $messageEloquent->update(
            [
                'text' => $message->getText(),
            ]
        );
    }

    public function store(MessageDTO $messageDTO): void
    {
        MessageEloquent::create(
            [
                'text' => $messageDTO->getText(),
            ]
        );

    }

    public function findById(int $id): Message
    {
        $messageEloquent = MessageEloquent::find($id);
        if (!$messageEloquent) {
            throw new MessageNotFoundException();
        }
        $message = $this->messagesFactory->makeFromEloquent($messageEloquent);

        return $message;
    }

}