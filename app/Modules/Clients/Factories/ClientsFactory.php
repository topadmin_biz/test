<?php

namespace App\Modules\Clients\Factories;

use App\Modules\Clients\Entities\Client;
use App\Modules\Clients\Entities\ClientDTO;
use Illuminate\Http\Request;
use App\Models\Client as ClientEloquent;

class ClientsFactory
{
    /**
     * @param ClientEloquent $client
     *
     * @return Client
     */
    public function makeFromEloquent(ClientEloquent $client): Client
    {
        $client = new Client(
            $client->id,
            $client->name
        );

        return $client;
    }

    /**
     * @param Request $request
     *
     * @return ClientDTO
     */
    public function makeDTOFromRequest(Request $request): ClientDTO
    {
        $clientDTO = new ClientDTO(
            $request->name
        );

        return $clientDTO;
    }
}
