<?php

namespace App\Modules\Clients;

use App\Modules\Clients\Entities\Client;
use Illuminate\Http\Request;

class ClientsHydrator
{
    public function fillFromRequest(Client $client, Request $request)
    {
        $client->setName($request->name);

        return $client;
    }
}
