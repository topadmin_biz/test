<?php

namespace App\Modules\Clients;

use App\Modules\Clients\Entities\Client;
use App\Modules\Clients\Entities\ClientDTO;

interface ClientsRepositoryInterface
{
    /**
     * @param Client $client
     */
    public function save(Client $client): void;

    /**
     * @param ClientDTO $clientDTO
     */
    public function store(ClientDTO $clientDTO): void;

    /**
     * @param int $id
     *
     * @return Client
     */
    public function findById(int $id): Client;
}
