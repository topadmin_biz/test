<?php

namespace App\Modules\Clients;

use App\Modules\Clients\Factories\ClientsFactory;
use Illuminate\Container\Container;
use Illuminate\Support\ServiceProvider;

class ClientsServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            ClientsRepositoryInterface::class,
            function (Container $app) {
                return new ClientsEloquentRepository(
                    $app->make(ClientsFactory::class)
                );
            }
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
