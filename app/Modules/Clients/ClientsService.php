<?php

namespace App\Modules\Clients;

use App\Modules\Clients\Entities\Client;
use App\Modules\Clients\Entities\ClientDTO;

class ClientsService
{
    /**
     * @var ClientsRepositoryInterface
     */
    private $clientsRepository;

    /**
     * ClientsService constructor.
     *
     * @param ClientsRepositoryInterface $clientsRepository
     */
    public function __construct(ClientsRepositoryInterface $clientsRepository)
    {
        $this->clientsRepository = $clientsRepository;
    }

    /**
     * @param Client $client
     */
    public function save(Client $client): void
    {
        $this->clientsRepository->save($client);
    }

    /**
     * @param ClientDTO $clientDTO
     */
    public function create(ClientDTO $clientDTO)
    {
        $this->clientsRepository->store($clientDTO);
    }

    /**
     * @param int $id
     *
     * @return Client
     */
    public function findById(int $id): Client
    {
        $Client = $this->clientsRepository->findById($id);

        return $Client;
    }
}
