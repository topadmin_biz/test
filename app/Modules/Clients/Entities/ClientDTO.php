<?php

namespace App\Modules\Clients\Entities;

class ClientDTO
{
    /**
     * @var string
     */
    private $name;

    /**
     * ClientDTO constructor.
     *
     * @param string $name
     */
    public function __construct(
        string $name
    ) {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
