<?php

namespace App\Modules\Clients;

use App\Modules\Clients\Entities\Client;
use App\Modules\Clients\Entities\ClientDTO;
use App\Modules\Clients\Exceptions\ClientNotFoundException;
use App\Modules\Clients\Factories\ClientsFactory;
use App\Models\Client as ClientEloquent;

class ClientsEloquentRepository implements ClientsRepositoryInterface
{
    /**
     * @var ClientsFactory
     */
    private $clientsFactory;

    /**
     * ClientsEloquentRepository constructor.
     *
     * @param ClientsFactory $clientsFactory
     */
    public function __construct(ClientsFactory $clientsFactory)
    {
        $this->clientsFactory = $clientsFactory;
    }

    /**
     * @param Client $client
     */
    public function save(Client $client): void
    {
        $clientEloquent = ClientEloquent::find($client->getId());
        $clientEloquent->update(
            [
                'name' => $client->getName(),
            ]
        );
    }

    /**
     * @param ClientDTO $clientDTO
     */
    public function store(ClientDTO $clientDTO): void
    {
        ClientEloquent::create(
            [
                'name' => $clientDTO->getName(),
            ]
        );
    }

    /**
     * @param int $id
     *
     * @return Client
     * @throws ClientNotFoundException
     */
    public function findById(int $id): Client
    {
        $clientEloquent = ClientEloquent::find($id);
        if (!$clientEloquent) {
            throw new ClientNotFoundException();
        }
        $client = $this->clientsFactory->makeFromEloquent($clientEloquent);

        return $client;
    }
}
