<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group(
    [
        'prefix' => config('admin.route.prefix'),
        'namespace' => config('admin.route.namespace'),
        'middleware' => config('admin.route.middleware'),
    ],
    function (Router $router) {
        $router->group(
            ['prefix' => 'auth'],
            function (Router $router) {
                $router->resource('permissions', 'PermissionController');
                $router->resource('roles', 'RoleController');
                $router->resource('users', 'ManagersController');
            }
        );
        $router->resource('sms', 'MessagesController');
        $router->resource('cities', 'CitiesController');
        $router->resource('categories', 'CategoriesController');
        $router->resource('products', 'ProductsController');
        $router->resource('clients', 'ClientsController');
        $router->get('/', 'HomeController@index');
    }
);
