<?php

namespace App\Admin\Controllers;

use App\Admin\Factories\CategoriesViewFactory;
use App\Http\Controllers\Controller;
use App\Modules\Categories\Factories\CategoriesFactory;
use App\Modules\Categories\CategoriesHydrator;
use App\Modules\Categories\CategoriesService;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    use HasResourceActions;
    /**
     * @var CategoriesViewFactory
     */
    private $categoriesViewFactory;
    /**
     * @var CategoriesService
     */
    private $categoriesService;
    /**
     * @var CategoriesHydrator
     */
    private $categoriesHydrator;
    /**
     * @var CategoriesFactory
     */
    private $categoriesFactory;

    /**
     * UserController constructor.
     *
     * @param CategoriesViewFactory $categoriesViewFactory
     * @param CategoriesService $categoriesService
     * @param CategoriesHydrator $categoriesHydrator
     * @param CategoriesFactory $categoriesFactory
     */
    public function __construct(
        CategoriesViewFactory $categoriesViewFactory,
        CategoriesService $categoriesService,
        CategoriesHydrator $categoriesHydrator,
        CategoriesFactory $categoriesFactory
    ) {
        $this->categoriesViewFactory = $categoriesViewFactory;
        $this->categoriesService = $categoriesService;
        $this->categoriesHydrator = $categoriesHydrator;
        $this->categoriesFactory = $categoriesFactory;
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return $this->categoriesViewFactory->index();
    }

    /**
     * Show interface.
     *
     * @param int $id
     *
     * @return Content
     */
    public function show(int $id)
    {
        return $this->categoriesViewFactory->show($id);
    }

    /**
     * Edit interface.
     *
     * @param int $id
     *
     * @return Content
     */
    public function edit(int $id)
    {
        return $this->categoriesViewFactory->edit($id);
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return $this->categoriesViewFactory->create();
    }

    /**
     * Create interface.
     */
    public function form()
    {
        return $this->categoriesViewFactory->form();
    }

    public function update(int $id, Request $request)
    {
        $this->validate(
            $request,
            [
                'name' => 'required',
            ]
        );
        $category = $this->categoriesService->findById($id);
        $category = $this->categoriesHydrator->fillFromRequest($category, $request);
        $this->categoriesService->save($category);

        return response()->updated('users');
    }

    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'name' => 'required',
            ]
        );
        $categoryDTO = $this->categoriesFactory->makeDTOFromRequest($request);
        $this->categoriesService->create($categoryDTO);

        return response()->updated('users');
    }
}
