<?php

namespace App\Admin\Controllers;

use App\Admin\Factories\ManagersViewFactory;
use App\Http\Controllers\Controller;
use App\Modules\Managers\Factories\ManagersFactory;
use App\Modules\Managers\ManagersHydrator;
use App\Modules\Managers\ManagersService;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;

class ManagersController extends Controller
{
    use HasResourceActions;
    /**
     * @var ManagersViewFactory
     */
    private $managersViewFactory;
    /**
     * @var ManagersService
     */
    private $managersService;
    /**
     * @var ManagersHydrator
     */
    private $managersHydrator;
    /**
     * @var ManagersFactory
     */
    private $managersFactory;

    /**
     * UserController constructor.
     *
     * @param ManagersViewFactory $managersViewFactory
     * @param ManagersService $managersService
     * @param ManagersHydrator $managersHydrator
     * @param ManagersFactory $managersFactory
     */
    public function __construct(
        ManagersViewFactory $managersViewFactory,
        ManagersService $managersService,
        ManagersHydrator $managersHydrator,
        ManagersFactory $managersFactory
    ) {
        $this->managersViewFactory = $managersViewFactory;
        $this->managersService = $managersService;
        $this->managersHydrator = $managersHydrator;
        $this->managersFactory = $managersFactory;
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return $this->managersViewFactory->index();
    }

    /**
     * Show interface.
     *
     * @param int $id
     *
     * @return Content
     */
    public function show(int $id)
    {
        return $this->managersViewFactory->show($id);
    }

    /**
     * Edit interface.
     *
     * @param int $id
     *
     * @return Content
     */
    public function edit(int $id)
    {
        return $this->managersViewFactory->edit($id);
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return $this->managersViewFactory->create();
    }

    /**
     * Create interface.
     */
    public function form()
    {
        return $this->managersViewFactory->form();
    }

    public function update(int $id, Request $request)
    {
        $this->validate(
            $request,
            [
                'username' => 'required',
                'name' => 'required',
                'password' => 'required|confirmed',
                'password_confirmation' => 'required',
            ]
        );
        $manager = $this->managersService->findById($id);
        $manager = $this->managersHydrator->fillFromRequest($manager, $request);
        $this->managersService->save($manager);

        return response()->updated('users');
    }

    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'username' => 'required',
                'name' => 'required',
                'password' => 'required|confirmed',
                'password_confirmation' => 'required',
            ]
        );
        $managerDTO = $this->managersFactory->makeDTOFromRequest($request);
        $this->managersService->create($managerDTO);

        return response()->updated('users');
    }
}
