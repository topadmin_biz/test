<?php

namespace App\Admin\Controllers;

use App\Admin\Factories\ClientsViewFactory;
use App\Http\Controllers\Controller;
use App\Modules\Clients\Factories\ClientsFactory;
use App\Modules\Clients\ClientsHydrator;
use App\Modules\Clients\ClientsService;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;

class ClientsController extends Controller
{
    use HasResourceActions;
    /**
     * @var ClientsViewFactory
     */
    private $clientsViewFactory;
    /**
     * @var ClientsService
     */
    private $clientsService;
    /**
     * @var ClientsHydrator
     */
    private $clientsHydrator;
    /**
     * @var ClientsFactory
     */
    private $clientsFactory;

    /**
     * UserController constructor.
     *
     * @param ClientsViewFactory $clientsViewFactory
     * @param ClientsService        $clientsService
     * @param ClientsHydrator       $clientsHydrator
     * @param ClientsFactory     $clientsFactory
     */
    public function __construct(
        ClientsViewFactory $clientsViewFactory,
        ClientsService $clientsService,
        ClientsHydrator $clientsHydrator,
        ClientsFactory $clientsFactory
    ) {
        $this->clientsViewFactory = $clientsViewFactory;
        $this->clientsService = $clientsService;
        $this->clientsHydrator = $clientsHydrator;
        $this->clientsFactory = $clientsFactory;
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return $this->clientsViewFactory->index();
    }

    /**
     * Show interface.
     *
     * @param int $id
     *
     * @return Content
     */
    public function show(int $id)
    {
        return $this->clientsViewFactory->show($id);
    }

    /**
     * Edit interface.
     *
     * @param int $id
     *
     * @return Content
     */
    public function edit(int $id)
    {
        return $this->clientsViewFactory->edit($id);
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return $this->clientsViewFactory->create();
    }

    /**
     * Create interface.
     */
    public function form()
    {
        return $this->clientsViewFactory->form();
    }

    public function update(int $id, Request $request)
    {
        $this->validate(
            $request,
            [
                'name' => 'required',
            ]
        );
        $client = $this->clientsService->findById($id);
        $client = $this->clientsHydrator->fillFromRequest($client, $request);
        $this->clientsService->save($client);

        return response()->updated('users');
    }

    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'name' => 'required',
            ]
        );
        $clientDTO = $this->clientsFactory->makeDTOFromRequest($request);
        $this->clientsService->create($clientDTO);

        return response()->updated('users');
    }
}
