<?php

namespace App\Admin\Controllers;

use App\Admin\Factories\RolesViewFactory;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Layout\Content;
use Illuminate\Routing\Controller;

class RoleController extends Controller
{
    use HasResourceActions;
    /**
     * @var RolesViewFactory
     */
    private $rolesViewFactory;

    /**
     * RoleController constructor.
     *
     * @param RolesViewFactory $rolesViewFactory
     */
    public function __construct(RolesViewFactory $rolesViewFactory)
    {
        $this->rolesViewFactory = $rolesViewFactory;
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return $this->rolesViewFactory->index();
    }

    /**
     * Show interface.
     *
     * @param int $id
     *
     * @return Content
     */
    public function show(int $id)
    {
        return $this->rolesViewFactory->show($id);
    }

    /**
     * Edit interface.
     *
     * @param int $id
     *
     * @return Content
     */
    public function edit(int $id)
    {
        return $this->rolesViewFactory->edit($id);
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return $this->rolesViewFactory->create();
    }

    public function form()
    {
        return $this->rolesViewFactory->form();
    }
}
