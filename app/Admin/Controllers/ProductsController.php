<?php

namespace App\Admin\Controllers;

use App\Admin\Factories\ProductsViewFactory;
use App\Http\Controllers\Controller;
use App\Modules\Products\Factories\ProductsFactory;
use App\Modules\Products\ProductEnum;
use App\Modules\Products\ProductsHydrator;
use App\Modules\Products\ProductsService;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ProductsController extends Controller
{
    use HasResourceActions;
    /**
     * @var ProductsViewFactory
     */
    private $productsViewFactory;
    /**
     * @var ProductsService
     */
    private $productsService;
    /**
     * @var ProductsHydrator
     */
    private $productsHydrator;
    /**
     * @var ProductsFactory
     */
    private $productsFactory;

    /**
     * UserController constructor.
     *
     * @param ProductsViewFactory $productsViewFactory
     * @param ProductsService $productsService
     * @param ProductsHydrator $productsHydrator
     * @param ProductsFactory $productsFactory
     */
    public function __construct(
        ProductsViewFactory $productsViewFactory,
        ProductsService $productsService,
        ProductsHydrator $productsHydrator,
        ProductsFactory $productsFactory
    ) {
        $this->productsViewFactory = $productsViewFactory;
        $this->productsService = $productsService;
        $this->productsHydrator = $productsHydrator;
        $this->productsFactory = $productsFactory;
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return $this->productsViewFactory->index();
    }

    /**
     * Show interface.
     *
     * @param int $id
     *
     * @return Content
     */
    public function show(int $id)
    {
        return $this->productsViewFactory->show($id);
    }

    /**
     * Edit interface.
     *
     * @param int $id
     *
     * @return Content
     */
    public function edit(int $id)
    {
        return $this->productsViewFactory->edit($id);
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return $this->productsViewFactory->create();
    }

    /**
     * Create interface.
     */
    public function form()
    {
        return $this->productsViewFactory->form();
    }

    public function update(int $id, Request $request)
    {
        $this->validate(
            $request,
            [
                'name' => 'required', //todo added class for validation rules
                'workabox_id' => 'required|integer',
                'link_to_review' => 'nullable',
                'price' => 'required|numeric',
                'status' => Rule::in(ProductEnum::STATUSES),
            ]
        );
        $product = $this->productsService->findById($id);
        $product = $this->productsHydrator->fillFromRequest($product, $request);
        $this->productsService->save($product);

        return response()->updated('users');
    }

    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'name' => 'required', //todo added class for validation rules
                'workabox_id' => 'required|integer',
                'link_to_review' => 'nullable',
                'price' => 'required|numeric',
                'status' => Rule::in(ProductEnum::STATUSES),
            ]
        );
        $productDTO = $this->productsFactory->makeDTOFromRequest($request);
        $this->productsService->create($productDTO);

        return response()->updated('users');
    }
}
