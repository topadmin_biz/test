<?php

namespace App\Admin\Controllers;

use App\Admin\Factories\PermissionsViewFactory;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Layout\Content;
use Illuminate\Routing\Controller;

class PermissionController extends Controller
{
    use HasResourceActions;
    /**
     * @var PermissionsViewFactory
     */
    private $permissionsViewFactory;

    /**
     * PermissionController constructor.
     *
     * @param PermissionsViewFactory $permissionsViewFactory
     */
    public function __construct(PermissionsViewFactory $permissionsViewFactory)
    {
        $this->permissionsViewFactory = $permissionsViewFactory;
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return $this->permissionsViewFactory->index();
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     *
     * @return Content
     */
    public function show(int $id)
    {
        return $this->permissionsViewFactory->show($id);
    }

    /**
     * Edit interface.
     *
     * @param $id
     *
     * @return Content
     */
    public function edit(int $id)
    {
        return $this->permissionsViewFactory->edit($id);
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return $this->permissionsViewFactory->create();
    }

    public function form()
    {
        return $this->permissionsViewFactory->form();
    }
}
