<?php

namespace App\Admin\Controllers;

use App\Admin\Factories\MessagesViewFactory;
use App\Http\Controllers\Controller;
use App\Modules\Messages\Factories\MessagesFactory;
use App\Modules\Messages\MessagesHydrator;
use App\Modules\Messages\MessagesService;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;

class MessagesController extends Controller
{
    use HasResourceActions;
    /**
     * @var MessagesViewFactory
     */
    private $messagesViewFactory;
    /**
     * @var MessagesService
     */
    private $messagesService;
    /**
     * @var MessagesHydrator
     */
    private $messagesHydrator;
    /**
     * @var MessagesFactory
     */
    private $messagesFactory;

    /**
     * UserController constructor.
     *
     * @param MessagesViewFactory $messagesViewFactory
     * @param MessagesService $messagesService
     * @param MessagesHydrator $messagesHydrator
     * @param MessagesFactory $messagesFactory
     */
    public function __construct(
        MessagesViewFactory $messagesViewFactory,
        MessagesService $messagesService,
        MessagesHydrator $messagesHydrator,
        MessagesFactory $messagesFactory
    ) {
        $this->messagesViewFactory = $messagesViewFactory;
        $this->messagesService = $messagesService;
        $this->messagesHydrator = $messagesHydrator;
        $this->messagesFactory = $messagesFactory;
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return $this->messagesViewFactory->index();
    }

    /**
     * Show interface.
     *
     * @param int $id
     *
     * @return Content
     */
    public function show(int $id)
    {
        return $this->messagesViewFactory->show($id);
    }

    /**
     * Edit interface.
     *
     * @param int $id
     *
     * @return Content
     */
    public function edit(int $id)
    {
        return $this->messagesViewFactory->edit($id);
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return $this->messagesViewFactory->create();
    }

    /**
     * Create interface.
     */
    public function form()
    {
        return $this->messagesViewFactory->form();
    }

    public function update(int $id, Request $request)
    {
        $this->validate(
            $request,
            [
                'text' => 'required',
            ]
        );
        $message = $this->messagesService->findById($id);
        $message = $this->messagesHydrator->fillFromRequest($message, $request);
        $this->messagesService->save($message);

        return response()->updated('sms');
    }

    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'text' => 'required',
            ]
        );
        $messageDTO = $this->messagesFactory->makeDTOFromRequest($request);
        $this->messagesService->create($messageDTO);

        return response()->updated('sms');
    }
}
