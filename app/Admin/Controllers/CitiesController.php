<?php

namespace App\Admin\Controllers;

use App\Admin\Factories\CitiesViewFactory;
use App\Http\Controllers\Controller;
use App\Modules\Cities\Factories\CitiesFactory;
use App\Modules\Cities\CitiesHydrator;
use App\Modules\Cities\CitiesService;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;

class CitiesController extends Controller
{
    use HasResourceActions;
    /**
     * @var CitiesViewFactory
     */
    private $citiesViewFactory;
    /**
     * @var CitiesService
     */
    private $citiesService;
    /**
     * @var CitiesHydrator
     */
    private $citiesHydrator;
    /**
     * @var CitiesFactory
     */
    private $citiesFactory;

    /**
     * UserController constructor.
     *
     * @param CitiesViewFactory $citiesViewFactory
     * @param CitiesService $citiesService
     * @param CitiesHydrator $citiesHydrator
     * @param CitiesFactory $citiesFactory
     */
    public function __construct(
        CitiesViewFactory $citiesViewFactory,
        CitiesService $citiesService,
        CitiesHydrator $citiesHydrator,
        CitiesFactory $citiesFactory
    ) {
        $this->citiesViewFactory = $citiesViewFactory;
        $this->citiesService = $citiesService;
        $this->citiesHydrator = $citiesHydrator;
        $this->citiesFactory = $citiesFactory;
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return $this->citiesViewFactory->index();
    }

    /**
     * Show interface.
     *
     * @param int $id
     *
     * @return Content
     */
    public function show(int $id)
    {
        return $this->citiesViewFactory->show($id);
    }

    /**
     * Edit interface.
     *
     * @param int $id
     *
     * @return Content
     */
    public function edit(int $id)
    {
        return $this->citiesViewFactory->edit($id);
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return $this->citiesViewFactory->create();
    }

    /**
     * Create interface.
     */
    public function form()
    {
        return $this->citiesViewFactory->form();
    }

    public function update(int $id, Request $request)
    {
        $this->validate(
            $request,
            [
                'name' => 'required',
            ]
        );
        $city = $this->citiesService->findById($id);
        $city = $this->citiesHydrator->fillFromRequest($city, $request);
        $this->citiesService->save($city);

        return response()->updated('users');
    }

    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'name' => 'required',
            ]
        );
        $cityDTO = $this->citiesFactory->makeDTOFromRequest($request);
        $this->citiesService->create($cityDTO);

        return response()->updated('users');
    }
}
