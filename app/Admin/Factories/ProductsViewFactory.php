<?php

namespace App\Admin\Factories;

use App\Modules\Products\ProductEnum;
use App\Modules\Products\ProductsService;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use App\Models\Product as ProductEloquent;

class ProductsViewFactory
{
    /**
     * @var ProductsService
     */
    private $categoriesService;

    /**
     * ManagersViewFactory constructor.
     *
     * @param ProductsService $categoriesService
     */
    public function __construct(ProductsService $categoriesService)
    {
        $this->categoriesService = $categoriesService;
    }

    /**
     * @return Content
     */
    public function index()
    {
        return Admin::content(
            function (Content $content) {
                $content
                    ->header(trans('Products'))
                    ->description(trans('admin.list'))
                    ->body($this->grid()->render());
            }
        );
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function show(int $id)
    {
        return Admin::content(
            function (Content $content) use ($id) {
                $content
                    ->header(trans('Products'))
                    ->description(trans('admin.detail'))
                    ->body($this->detail($id));
            }
        );
    }

    /**
     * @param int $id
     *
     * @return Content
     */
    public function edit(int $id)
    {
        return Admin::content(
            function (Content $content) use ($id) {
                $content
                    ->header(trans('Products'))
                    ->description(trans('admin.edit'))
                    ->body($this->editForm($id));
            }
        );
    }

    /**
     * @return Content
     */
    public function create()
    {
        return Admin::content(
            function (Content $content) {
                $content
                    ->header(trans('Products'))
                    ->description(trans('admin.create'))
                    ->body($this->form());
            }
        );
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ProductEloquent());

        $grid->id('ID')->sortable();
        $grid->name(trans('admin.name'));

        $grid->tools(
            function (Grid\Tools $tools) {
                $tools->batch(
                    function (Grid\Tools\BatchActions $actions) {
                        $actions->disableDelete();
                    }
                );
            }
        );

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ProductEloquent::findOrFail($id));

        $show->id('ID');
        $show->name(trans('admin.name'));
        $show->workabox_id();
        $show->link_to_review();
        $show->created_at(trans('admin.created_at'));
        $show->updated_at(trans('admin.updated_at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form()
    {
        $form = new Form(new ProductEloquent());

        $form->display('id', 'ID');

        $form->text('name', trans('admin.name'));
        $form->number('workabox_id');
        $form->text('link_to_review');
        $form->currency('price');
        $form->select('status_if_ended')->options(ProductEnum::STATUS_LIST);

        return $form;
    }

    /**
     * Make a form builder.
     *
     * @param int $id
     *
     * @return Form
     */
    public function editForm(int $id)
    {
        $product = $this->categoriesService->findById($id);

        $form = new Form(new ProductEloquent());
        $form->hidden('_method')->value('PUT');

        $form->display('id', 'ID');

        $form->text('name', trans('admin.name'))->value($product->getName());
        $form->number('workabox_id')->value($product->getWorkaboxId());
        $form->text('link_to_review')->value($product->getLinkToReview());
        $form->currency('price')->value($product->getPrice()->getAmount());
        $form->select('status_if_ended')->options(ProductEnum::STATUS_LIST)->value($product->getStatusIfEnded());

        return $form;
    }
}