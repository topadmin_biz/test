<?php

namespace App\Admin\Factories;

use App\Modules\Cities\CitiesService;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use App\Models\City as CityEloquent;

class CitiesViewFactory
{
    /**
     * @var CitiesService
     */
    private $citiesService;

    /**
     * CitiesViewFactory constructor.
     *
     * @param CitiesService $citiesService
     */
    public function __construct(CitiesService $citiesService)
    {
        $this->citiesService = $citiesService;
    }

    /**
     * @return Content
     */
    public function index()
    {
        return Admin::content(
            function (Content $content) {
                $content
                    ->header(trans('Cities'))
                    ->description(trans('admin.list'))
                    ->body($this->grid()->render());
            }
        );
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function show(int $id)
    {
        return Admin::content(
            function (Content $content) use ($id) {
                $content
                    ->header(trans('Cities'))
                    ->description(trans('admin.detail'))
                    ->body($this->detail($id));
            }
        );
    }

    /**
     * @param int $id
     *
     * @return Content
     */
    public function edit(int $id)
    {
        return Admin::content(
            function (Content $content) use ($id) {
                $content
                    ->header(trans('Cities'))
                    ->description(trans('admin.edit'))
                    ->body($this->editForm($id));
            }
        );
    }

    /**
     * @return Content
     */
    public function create()
    {
        return Admin::content(
            function (Content $content) {
                $content
                    ->header(trans('Cities'))
                    ->description(trans('admin.create'))
                    ->body($this->form());
            }
        );
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new CityEloquent());

        $grid->id('ID')->sortable();
        $grid->name(trans('admin.name'));
        $grid->created_at(trans('admin.created_at'));
        $grid->updated_at(trans('admin.updated_at'));

        $grid->tools(
            function (Grid\Tools $tools) {
                $tools->batch(
                    function (Grid\Tools\BatchActions $actions) {
                        $actions->disableDelete();
                    }
                );
            }
        );

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(CityEloquent::findOrFail($id));

        $show->id('ID');
        $show->name(trans('admin.name'));
        $show->created_at(trans('admin.created_at'));
        $show->updated_at(trans('admin.updated_at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form()
    {
        $form = new Form(new CityEloquent());

        $form->display('id', 'ID');

        $form->text('name', trans('admin.name'));

        return $form;
    }

    /**
     * Make a form builder.
     *
     * @param int $id
     *
     * @return Form
     */
    public function editForm(int $id)
    {
        $city = $this->citiesService->findById($id);

        $form = new Form(new CityEloquent());
        $form->hidden('_method')->value('PUT');

        $form->display('id', 'ID');

        $form->text('name', trans('admin.name'))->value($city->getName());

        return $form;
    }
}