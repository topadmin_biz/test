<?php

namespace App\Admin\Factories;

use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Support\Str;

class RolesViewFactory
{
    /**
     * @return Content
     */
    public function index()
    {
        return Admin::content(
            function (Content $content) {
                $content
                    ->header(trans('admin.permissions'))
                    ->description(trans('admin.list'))
                    ->body($this->grid()->render());
            }
        );
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function show(int $id)
    {
        return Admin::content(
            function (Content $content) use ($id) {
                $content
                    ->header(trans('admin.administrator'))
                    ->description(trans('admin.detail'))
                    ->body($this->detail($id));
            }
        );
    }

    /**
     * @param int $id
     *
     * @return Content
     */
    public function edit(int $id)
    {
        return Admin::content(
            function (Content $content) use ($id) {
                $content
                    ->header(trans('admin.administrator'))
                    ->description(trans('admin.edit'))
                    ->body($this->form()->edit($id));
            }
        );
    }

    /**
     * @return Content
     */
    public function create()
    {
        return Admin::content(
            function (Content $content) {
                $content
                    ->header(trans('admin.administrator'))
                    ->description(trans('admin.create'))
                    ->body($this->form());
            }
        );
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $roleModel = config('admin.database.roles_model');

        $grid = new Grid(new $roleModel());

        $grid->id('ID')->sortable();
        $grid->slug(trans('admin.slug'));
        $grid->name(trans('admin.name'));

        $grid->permissions(trans('admin.permission'))->pluck('name')->label();

        $grid->created_at(trans('admin.created_at'));
        $grid->updated_at(trans('admin.updated_at'));

        $grid->actions(
            function (Grid\Displayers\Actions $actions) {
                if ($actions->row->slug == 'administrator') {
                    $actions->disableDelete();
                }
            }
        );

        $grid->tools(
            function (Grid\Tools $tools) {
                $tools->batch(
                    function (Grid\Tools\BatchActions $actions) {
                        $actions->disableDelete();
                    }
                );
            }
        );

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        $roleModel = config('admin.database.roles_model');

        $show = new Show($roleModel::findOrFail($id));

        $show->id('ID');
        $show->slug(trans('admin.slug'));
        $show->name(trans('admin.name'));
        $show->permissions(trans('admin.permissions'))->as(
            function ($permission) {
                return $permission->pluck('name');
            }
        )->label();
        $show->created_at(trans('admin.created_at'));
        $show->updated_at(trans('admin.updated_at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form()
    {
        $permissionModel = config('admin.database.permissions_model');
        $roleModel = config('admin.database.roles_model');

        $form = new Form(new $roleModel());

        $form->display('id', 'ID');

        $form->text('slug', trans('admin.slug'))->rules('required');
        $form->text('name', trans('admin.name'))->rules('required');
        $form->listbox('permissions', trans('admin.permissions'))->options(
            $permissionModel::all()->pluck('name', 'id')
        );

        $form->display('created_at', trans('admin.created_at'));
        $form->display('updated_at', trans('admin.updated_at'));

        return $form;
    }
}