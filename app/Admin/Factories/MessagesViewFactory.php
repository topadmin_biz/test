<?php

namespace App\Admin\Factories;

use App\Modules\Messages\MessagesService;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use App\Models\Message as MessageEloquent;

class MessagesViewFactory
{
    /**
     * @var MessagesService
     */
    private $messagesService;

    /**
     * ManagersViewFactory constructor.
     *
     * @param MessagesService $messagesService
     */
    public function __construct(MessagesService $messagesService)
    {
        $this->messagesService = $messagesService;
    }

    /**
     * @return Content
     */
    public function index()
    {
        return Admin::content(
            function (Content $content) {
                $content
                    ->header(trans('Sms'))
                    ->description(trans('admin.list'))
                    ->body($this->grid()->render());
            }
        );
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function show(int $id)
    {
        return Admin::content(
            function (Content $content) use ($id) {
                $content
                    ->header(trans('Sms'))
                    ->description(trans('admin.detail'))
                    ->body($this->detail($id));
            }
        );
    }

    /**
     * @param int $id
     *
     * @return Content
     */
    public function edit(int $id)
    {
        return Admin::content(
            function (Content $content) use ($id) {
                $content
                    ->header(trans('Sms'))
                    ->description(trans('admin.edit'))
                    ->body($this->editForm($id));
            }
        );
    }

    /**
     * @return Content
     */
    public function create()
    {
        return Admin::content(
            function (Content $content) {
                $content
                    ->header(trans('Sms'))
                    ->description(trans('admin.create'))
                    ->body($this->form());
            }
        );
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new MessageEloquent());

        $grid->id('ID')->sortable();
        $grid->text(trans('admin.text'));
        $grid->created_at(trans('admin.created_at'));
        $grid->updated_at(trans('admin.updated_at'));

        $grid->tools(
            function (Grid\Tools $tools) {
                $tools->batch(
                    function (Grid\Tools\BatchActions $actions) {
                        $actions->disableDelete();
                    }
                );
            }
        );

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(MessageEloquent::findOrFail($id));

        $show->id('ID');
        $show->text(trans('admin.text'));
        $show->created_at(trans('admin.created_at'));
        $show->updated_at(trans('admin.updated_at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form()
    {
        $form = new Form(new MessageEloquent());

        $form->display('id', 'ID');

        $form->text('text', trans('admin.text'));

        return $form;
    }

    /**
     * Make a form builder.
     *
     * @param int $id
     *
     * @return Form
     */
    public function editForm(int $id)
    {
        $message = $this->messagesService->findById($id);

        $form = new Form(new MessageEloquent());
        $form->hidden('_method')->value('PUT');

        $form->display('id', 'ID');

        $form->text('text', trans('admin.text'))->value($message->getText());

        return $form;
    }
}