<?php

namespace App\Admin\Factories;

use App\Modules\Managers\ManagersService;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ManagersViewFactory
{
    /**
     * @var ManagersService
     */
    private $managersService;

    /**
     * ManagersViewFactory constructor.
     *
     * @param ManagersService $managersService
     */
    public function __construct(ManagersService $managersService)
    {
        $this->managersService = $managersService;
    }

    /**
     * @return Content
     */
    public function index()
    {
        return Admin::content(
            function (Content $content) {
                $content
                    ->header(trans('admin.administrator'))
                    ->description(trans('admin.list'))
                    ->body($this->grid()->render());
            }
        );
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function show(int $id)
    {
        return Admin::content(
            function (Content $content) use ($id) {
                $content
                    ->header(trans('admin.administrator'))
                    ->description(trans('admin.detail'))
                    ->body($this->detail($id));
            }
        );
    }

    /**
     * @param int $id
     *
     * @return Content
     */
    public function edit(int $id)
    {
        return Admin::content(
            function (Content $content) use ($id) {
                $content
                    ->header(trans('admin.administrator'))
                    ->description(trans('admin.edit'))
                    ->body($this->editForm($id));
            }
        );
    }

    /**
     * @return Content
     */
    public function create()
    {
        return Admin::content(
            function (Content $content) {
                $content
                    ->header(trans('admin.administrator'))
                    ->description(trans('admin.create'))
                    ->body($this->form());
            }
        );
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $userModel = config('admin.database.users_model');

        $grid = new Grid(new $userModel());

        $grid->id('ID')->sortable();
        $grid->username(trans('admin.username'));
        $grid->name(trans('admin.name'));
        $grid->roles(trans('admin.roles'))->pluck('name')->label();
        $grid->created_at(trans('admin.created_at'));
        $grid->updated_at(trans('admin.updated_at'));

        $grid->actions(
            function (Grid\Displayers\Actions $actions) {
                if ($actions->getKey() == 1) {
                    $actions->disableDelete();
                }
            }
        );

        $grid->tools(
            function (Grid\Tools $tools) {
                $tools->batch(
                    function (Grid\Tools\BatchActions $actions) {
                        $actions->disableDelete();
                    }
                );
            }
        );

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        $userModel = config('admin.database.users_model');

        $show = new Show($userModel::findOrFail($id));

        $show->id('ID');
        $show->username(trans('admin.username'));
        $show->name(trans('admin.name'));
        $show->roles(trans('admin.roles'))->as(
            function ($roles) {
                return $roles->pluck('name');
            }
        )->label();
        $show->permissions(trans('admin.permissions'))->as(
            function ($permission) {
                return $permission->pluck('name');
            }
        )->label();
        $show->created_at(trans('admin.created_at'));
        $show->updated_at(trans('admin.updated_at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form()
    {
        $userModel = config('admin.database.users_model');
        $permissionModel = config('admin.database.permissions_model');
        $roleModel = config('admin.database.roles_model');

        $form = new Form(new $userModel());

        $form->display('id', 'ID');

        $form->text('username', trans('admin.username'));
        $form->text('name', trans('admin.name'));
        $form->password('password', trans('admin.password'));
        $form->password('password_confirmation', trans('admin.password_confirmation'))
            ->default(
                function ($form) {
                    return $form->model()->password;
                }
            );

        $form->ignore(['password_confirmation']);

        $form->multipleSelect('roles', trans('admin.roles'))->options($roleModel::all()->pluck('name', 'id'));
        $form->multipleSelect('permissions', trans('admin.permissions'))->options(
            $permissionModel::all()->pluck('name', 'id')
        );

        return $form;
    }

    /**
     * Make a form builder.
     *
     * @param int $id
     *
     * @return Form
     */
    public function editForm(int $id)
    {
        $manager = $this->managersService->findById($id);
        $userModel = config('admin.database.users_model');
        $permissionModel = config('admin.database.permissions_model');
        $roleModel = config('admin.database.roles_model');

        $form = new Form(new $userModel());
        $form->hidden('_method')->value('PUT');

        $form->display('id', 'ID');

        $form->text('username', trans('admin.username'))->value($manager->getUsername());
        $form->text('name', trans('admin.name'))->value($manager->getName());
        $form->password('password', trans('admin.password'))->value($manager->getPassword());
        $form->password('password_confirmation', trans('admin.password_confirmation'))->value($manager->getPassword());

        $form->ignore(['password_confirmation']);

        $form->multipleSelect('roles', trans('admin.roles'))
            ->options($roleModel::all()->pluck('name', 'id'))
            ->value($manager->getRoles());

        $form->multipleSelect('permissions', trans('admin.permissions'))
            ->options($permissionModel::all()->pluck('name', 'id'))
            ->value($manager->getPermissions());

        return $form;
    }
}