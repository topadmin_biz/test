<?php
namespace Workabox\API;

/**
 * Autogenerated by Thrift Compiler (0.12.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
use Thrift\Base\TBase;
use Thrift\Type\TType;
use Thrift\Type\TMessageType;
use Thrift\Exception\TException;
use Thrift\Exception\TProtocolException;
use Thrift\Protocol\TProtocol;
use Thrift\Protocol\TBinaryProtocolAccelerated;
use Thrift\Exception\TApplicationException;

class Inventory
{
    static public $isValidate = false;

    static public $_TSPEC = array(
        1 => array(
            'var' => 'ID',
            'isRequired' => false,
            'type' => TType::I32,
        ),
        2 => array(
            'var' => 'OuterID',
            'isRequired' => false,
            'type' => TType::STRING,
        ),
        3 => array(
            'var' => 'Additional',
            'isRequired' => false,
            'type' => TType::STRING,
        ),
        4 => array(
            'var' => 'DocumentSumm',
            'isRequired' => false,
            'type' => TType::DOUBLE,
        ),
        5 => array(
            'var' => 'Responsible',
            'isRequired' => false,
            'type' => TType::STRUCT,
            'class' => '\Workabox\API\ObjectID',
        ),
        6 => array(
            'var' => 'InventoryType',
            'isRequired' => false,
            'type' => TType::STRING,
        ),
        7 => array(
            'var' => 'Store',
            'isRequired' => false,
            'type' => TType::STRUCT,
            'class' => '\Workabox\API\ObjectID',
        ),
        8 => array(
            'var' => 'GoodsInInventory',
            'isRequired' => false,
            'type' => TType::LST,
            'etype' => TType::STRUCT,
            'elem' => array(
                'type' => TType::STRUCT,
                'class' => '\Workabox\API\GoodsInInventoryLine',
                ),
        ),
        9 => array(
            'var' => 'DocNumber',
            'isRequired' => false,
            'type' => TType::STRING,
        ),
        10 => array(
            'var' => 'DocumentDate',
            'isRequired' => false,
            'type' => TType::STRING,
        ),
        11 => array(
            'var' => 'OuterCodes',
            'isRequired' => false,
            'type' => TType::LST,
            'etype' => TType::STRUCT,
            'elem' => array(
                'type' => TType::STRUCT,
                'class' => '\Workabox\API\OuterCodesLine',
                ),
        ),
        12 => array(
            'var' => 'StateInventory',
            'isRequired' => false,
            'type' => TType::STRING,
        ),
    );

    /**
     * @var int
     */
    public $ID = null;
    /**
     * @var string
     */
    public $OuterID = null;
    /**
     * @var string
     */
    public $Additional = null;
    /**
     * @var double
     */
    public $DocumentSumm = null;
    /**
     * @var \Workabox\API\ObjectID
     */
    public $Responsible = null;
    /**
     * @var string
     */
    public $InventoryType = null;
    /**
     * @var \Workabox\API\ObjectID
     */
    public $Store = null;
    /**
     * @var \Workabox\API\GoodsInInventoryLine[]
     */
    public $GoodsInInventory = null;
    /**
     * @var string
     */
    public $DocNumber = null;
    /**
     * @var string
     */
    public $DocumentDate = null;
    /**
     * @var \Workabox\API\OuterCodesLine[]
     */
    public $OuterCodes = null;
    /**
     * @var string
     */
    public $StateInventory = null;

    public function __construct($vals = null)
    {
        if (is_array($vals)) {
            if (isset($vals['ID'])) {
                $this->ID = $vals['ID'];
            }
            if (isset($vals['OuterID'])) {
                $this->OuterID = $vals['OuterID'];
            }
            if (isset($vals['Additional'])) {
                $this->Additional = $vals['Additional'];
            }
            if (isset($vals['DocumentSumm'])) {
                $this->DocumentSumm = $vals['DocumentSumm'];
            }
            if (isset($vals['Responsible'])) {
                $this->Responsible = $vals['Responsible'];
            }
            if (isset($vals['InventoryType'])) {
                $this->InventoryType = $vals['InventoryType'];
            }
            if (isset($vals['Store'])) {
                $this->Store = $vals['Store'];
            }
            if (isset($vals['GoodsInInventory'])) {
                $this->GoodsInInventory = $vals['GoodsInInventory'];
            }
            if (isset($vals['DocNumber'])) {
                $this->DocNumber = $vals['DocNumber'];
            }
            if (isset($vals['DocumentDate'])) {
                $this->DocumentDate = $vals['DocumentDate'];
            }
            if (isset($vals['OuterCodes'])) {
                $this->OuterCodes = $vals['OuterCodes'];
            }
            if (isset($vals['StateInventory'])) {
                $this->StateInventory = $vals['StateInventory'];
            }
        }
    }

    public function getName()
    {
        return 'Inventory';
    }


    public function read($input)
    {
        $xfer = 0;
        $fname = null;
        $ftype = 0;
        $fid = 0;
        $xfer += $input->readStructBegin($fname);
        while (true) {
            $xfer += $input->readFieldBegin($fname, $ftype, $fid);
            if ($ftype == TType::STOP) {
                break;
            }
            switch ($fid) {
                case 1:
                    if ($ftype == TType::I32) {
                        $xfer += $input->readI32($this->ID);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 2:
                    if ($ftype == TType::STRING) {
                        $xfer += $input->readString($this->OuterID);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 3:
                    if ($ftype == TType::STRING) {
                        $xfer += $input->readString($this->Additional);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 4:
                    if ($ftype == TType::DOUBLE) {
                        $xfer += $input->readDouble($this->DocumentSumm);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 5:
                    if ($ftype == TType::STRUCT) {
                        $this->Responsible = new \Workabox\API\ObjectID();
                        $xfer += $this->Responsible->read($input);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 6:
                    if ($ftype == TType::STRING) {
                        $xfer += $input->readString($this->InventoryType);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 7:
                    if ($ftype == TType::STRUCT) {
                        $this->Store = new \Workabox\API\ObjectID();
                        $xfer += $this->Store->read($input);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 8:
                    if ($ftype == TType::LST) {
                        $this->GoodsInInventory = array();
                        $_size847 = 0;
                        $_etype850 = 0;
                        $xfer += $input->readListBegin($_etype850, $_size847);
                        for ($_i851 = 0; $_i851 < $_size847; ++$_i851) {
                            $elem852 = null;
                            $elem852 = new \Workabox\API\GoodsInInventoryLine();
                            $xfer += $elem852->read($input);
                            $this->GoodsInInventory []= $elem852;
                        }
                        $xfer += $input->readListEnd();
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 9:
                    if ($ftype == TType::STRING) {
                        $xfer += $input->readString($this->DocNumber);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 10:
                    if ($ftype == TType::STRING) {
                        $xfer += $input->readString($this->DocumentDate);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 11:
                    if ($ftype == TType::LST) {
                        $this->OuterCodes = array();
                        $_size853 = 0;
                        $_etype856 = 0;
                        $xfer += $input->readListBegin($_etype856, $_size853);
                        for ($_i857 = 0; $_i857 < $_size853; ++$_i857) {
                            $elem858 = null;
                            $elem858 = new \Workabox\API\OuterCodesLine();
                            $xfer += $elem858->read($input);
                            $this->OuterCodes []= $elem858;
                        }
                        $xfer += $input->readListEnd();
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 12:
                    if ($ftype == TType::STRING) {
                        $xfer += $input->readString($this->StateInventory);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                default:
                    $xfer += $input->skip($ftype);
                    break;
            }
            $xfer += $input->readFieldEnd();
        }
        $xfer += $input->readStructEnd();
        return $xfer;
    }

    public function write($output)
    {
        $xfer = 0;
        $xfer += $output->writeStructBegin('Inventory');
        if ($this->ID !== null) {
            $xfer += $output->writeFieldBegin('ID', TType::I32, 1);
            $xfer += $output->writeI32($this->ID);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->OuterID !== null) {
            $xfer += $output->writeFieldBegin('OuterID', TType::STRING, 2);
            $xfer += $output->writeString($this->OuterID);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->Additional !== null) {
            $xfer += $output->writeFieldBegin('Additional', TType::STRING, 3);
            $xfer += $output->writeString($this->Additional);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->DocumentSumm !== null) {
            $xfer += $output->writeFieldBegin('DocumentSumm', TType::DOUBLE, 4);
            $xfer += $output->writeDouble($this->DocumentSumm);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->Responsible !== null) {
            if (!is_object($this->Responsible)) {
                throw new TProtocolException('Bad type in structure.', TProtocolException::INVALID_DATA);
            }
            $xfer += $output->writeFieldBegin('Responsible', TType::STRUCT, 5);
            $xfer += $this->Responsible->write($output);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->InventoryType !== null) {
            $xfer += $output->writeFieldBegin('InventoryType', TType::STRING, 6);
            $xfer += $output->writeString($this->InventoryType);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->Store !== null) {
            if (!is_object($this->Store)) {
                throw new TProtocolException('Bad type in structure.', TProtocolException::INVALID_DATA);
            }
            $xfer += $output->writeFieldBegin('Store', TType::STRUCT, 7);
            $xfer += $this->Store->write($output);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->GoodsInInventory !== null) {
            if (!is_array($this->GoodsInInventory)) {
                throw new TProtocolException('Bad type in structure.', TProtocolException::INVALID_DATA);
            }
            $xfer += $output->writeFieldBegin('GoodsInInventory', TType::LST, 8);
            $output->writeListBegin(TType::STRUCT, count($this->GoodsInInventory));
            foreach ($this->GoodsInInventory as $iter859) {
                $xfer += $iter859->write($output);
            }
            $output->writeListEnd();
            $xfer += $output->writeFieldEnd();
        }
        if ($this->DocNumber !== null) {
            $xfer += $output->writeFieldBegin('DocNumber', TType::STRING, 9);
            $xfer += $output->writeString($this->DocNumber);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->DocumentDate !== null) {
            $xfer += $output->writeFieldBegin('DocumentDate', TType::STRING, 10);
            $xfer += $output->writeString($this->DocumentDate);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->OuterCodes !== null) {
            if (!is_array($this->OuterCodes)) {
                throw new TProtocolException('Bad type in structure.', TProtocolException::INVALID_DATA);
            }
            $xfer += $output->writeFieldBegin('OuterCodes', TType::LST, 11);
            $output->writeListBegin(TType::STRUCT, count($this->OuterCodes));
            foreach ($this->OuterCodes as $iter860) {
                $xfer += $iter860->write($output);
            }
            $output->writeListEnd();
            $xfer += $output->writeFieldEnd();
        }
        if ($this->StateInventory !== null) {
            $xfer += $output->writeFieldBegin('StateInventory', TType::STRING, 12);
            $xfer += $output->writeString($this->StateInventory);
            $xfer += $output->writeFieldEnd();
        }
        $xfer += $output->writeFieldStop();
        $xfer += $output->writeStructEnd();
        return $xfer;
    }
}
