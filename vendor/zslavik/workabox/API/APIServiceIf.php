<?php
namespace Workabox\API;

/**
 * Autogenerated by Thrift Compiler (0.12.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
use Thrift\Base\TBase;
use Thrift\Type\TType;
use Thrift\Type\TMessageType;
use Thrift\Exception\TException;
use Thrift\Exception\TProtocolException;
use Thrift\Protocol\TProtocol;
use Thrift\Protocol\TBinaryProtocolAccelerated;
use Thrift\Exception\TApplicationException;

interface APIServiceIf
{
    /**
     * @param string $login
     * @param string $password
     * @return string
     * @throws \Workabox\API\APIException
     */
    public function Login($login, $password);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\GoodsGroupFilter $filter
     * @return \Workabox\API\GoodsGroup[]
     * @throws \Workabox\API\APIException
     */
    public function ExportGoodsGroup($apiKey, $date, \Workabox\API\GoodsGroupFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\HeadOffice $importdata
     * @throws \Workabox\API\APIException
     */
    public function ImportHeadOffice($apiKey, \Workabox\API\HeadOffice $importdata);
    /**
     * @param string $apiKey
     * @return \Workabox\API\HeadOffice
     * @throws \Workabox\API\APIException
     */
    public function ExportHeadOffice($apiKey);
    /**
     * @param string $apiKey
     * @param \Workabox\API\ObjectID $rttID
     * @return \Workabox\API\ObjectID[]
     * @throws \Workabox\API\APIException
     */
    public function ExportChosenGoods($apiKey, \Workabox\API\ObjectID $rttID);
    /**
     * @param string $apiKey
     * @param \Workabox\API\ObjectID $rttID
     * @return \Workabox\API\PricesListLine[]
     * @throws \Workabox\API\APIException
     */
    public function ExportChosenGoodsPrice($apiKey, \Workabox\API\ObjectID $rttID);
    /**
     * @param string $apiKey
     * @param \Workabox\API\ObjectID $rttID
     * @param \Workabox\API\ObjectID[] $goodsIDs
     * @throws \Workabox\API\APIException
     */
    public function ImportChosenGoods($apiKey, \Workabox\API\ObjectID $rttID, array $goodsIDs);
    /**
     * @param string $apiKey
     * @param \Workabox\API\ObjectID $rttID
     * @return \Workabox\API\PriceListRTTLine[]
     * @throws \Workabox\API\APIException
     */
    public function ExportPriceListRTT($apiKey, \Workabox\API\ObjectID $rttID);
    /**
     * @param string $apiKey
     * @param \Workabox\API\ObjectID $storeID
     * @return \Workabox\API\GoodsRestLine[]
     * @throws \Workabox\API\APIException
     */
    public function ExportGoodsQuantity($apiKey, \Workabox\API\ObjectID $storeID);
    /**
     * @param string $apiKey
     * @param \Workabox\API\CalculateSaleInfo $sale
     * @return \Workabox\API\CalculateSaleInfo
     * @throws \Workabox\API\APIException
     */
    public function CalculateSale($apiKey, \Workabox\API\CalculateSaleInfo $sale);
    /**
     * @param string $apiKey
     * @param \Workabox\API\CalculateOrderInfo $order
     * @return \Workabox\API\CalculateOrderInfo
     * @throws \Workabox\API\APIException
     */
    public function CalculateOrder($apiKey, \Workabox\API\CalculateOrderInfo $order);
    /**
     * @param string $apiKey
     * @param int $saleID
     * @throws \Workabox\API\APIException
     */
    public function CompleteSale($apiKey, $saleID);
    /**
     * @param string $apiKey
     * @param \Workabox\API\UserCatalog $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportUserCatalog($apiKey, \Workabox\API\UserCatalog $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\UserCatalogFilter $filter
     * @return \Workabox\API\UserCatalog[]
     * @throws \Workabox\API\APIException
     */
    public function ExportUserCatalog($apiKey, $date, \Workabox\API\UserCatalogFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\Goods $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportGoods($apiKey, \Workabox\API\Goods $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\GoodsFilter $filter
     * @return \Workabox\API\Goods[]
     * @throws \Workabox\API\APIException
     */
    public function ExportGoods($apiKey, $date, \Workabox\API\GoodsFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\Service $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportService($apiKey, \Workabox\API\Service $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\ServiceFilter $filter
     * @return \Workabox\API\Service[]
     * @throws \Workabox\API\APIException
     */
    public function ExportService($apiKey, $date, \Workabox\API\ServiceFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\MoneyFacilitiesType $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportMoneyFacilitiesType($apiKey, \Workabox\API\MoneyFacilitiesType $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\MoneyFacilitiesTypeFilter $filter
     * @return \Workabox\API\MoneyFacilitiesType[]
     * @throws \Workabox\API\APIException
     */
    public function ExportMoneyFacilitiesType($apiKey, $date, \Workabox\API\MoneyFacilitiesTypeFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\PriceList $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportPriceList($apiKey, \Workabox\API\PriceList $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\PriceListFilter $filter
     * @return \Workabox\API\PriceList[]
     * @throws \Workabox\API\APIException
     */
    public function ExportPriceList($apiKey, $date, \Workabox\API\PriceListFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\Supplier $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportSupplier($apiKey, \Workabox\API\Supplier $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\SupplierFilter $filter
     * @return \Workabox\API\Supplier[]
     * @throws \Workabox\API\APIException
     */
    public function ExportSupplier($apiKey, $date, \Workabox\API\SupplierFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\Office $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportOffice($apiKey, \Workabox\API\Office $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\OfficeFilter $filter
     * @return \Workabox\API\Office[]
     * @throws \Workabox\API\APIException
     */
    public function ExportOffice($apiKey, $date, \Workabox\API\OfficeFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\Stock $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportStock($apiKey, \Workabox\API\Stock $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\StockFilter $filter
     * @return \Workabox\API\Stock[]
     * @throws \Workabox\API\APIException
     */
    public function ExportStock($apiKey, $date, \Workabox\API\StockFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\RTT $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportRTT($apiKey, \Workabox\API\RTT $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\RTTFilter $filter
     * @return \Workabox\API\RTT[]
     * @throws \Workabox\API\APIException
     */
    public function ExportRTT($apiKey, $date, \Workabox\API\RTTFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\CashDeskRTT $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportCashDeskRTT($apiKey, \Workabox\API\CashDeskRTT $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\CashDeskRTTFilter $filter
     * @return \Workabox\API\CashDeskRTT[]
     * @throws \Workabox\API\APIException
     */
    public function ExportCashDeskRTT($apiKey, $date, \Workabox\API\CashDeskRTTFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\Employee $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportEmployee($apiKey, \Workabox\API\Employee $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\EmployeeFilter $filter
     * @return \Workabox\API\Employee[]
     * @throws \Workabox\API\APIException
     */
    public function ExportEmployee($apiKey, $date, \Workabox\API\EmployeeFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\Bank $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportBank($apiKey, \Workabox\API\Bank $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\BankFilter $filter
     * @return \Workabox\API\Bank[]
     * @throws \Workabox\API\APIException
     */
    public function ExportBank($apiKey, $date, \Workabox\API\BankFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\Account $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportAccount($apiKey, \Workabox\API\Account $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\AccountFilter $filter
     * @return \Workabox\API\Account[]
     * @throws \Workabox\API\APIException
     */
    public function ExportAccount($apiKey, $date, \Workabox\API\AccountFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\CashDesk $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportCashDesk($apiKey, \Workabox\API\CashDesk $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\CashDeskFilter $filter
     * @return \Workabox\API\CashDesk[]
     * @throws \Workabox\API\APIException
     */
    public function ExportCashDesk($apiKey, $date, \Workabox\API\CashDeskFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\MarketingInstrumentType $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportMarketingInstrumentType($apiKey, \Workabox\API\MarketingInstrumentType $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\MarketingInstrumentTypeFilter $filter
     * @return \Workabox\API\MarketingInstrumentType[]
     * @throws \Workabox\API\APIException
     */
    public function ExportMarketingInstrumentType($apiKey, $date, \Workabox\API\MarketingInstrumentTypeFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\ActualizingMI $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportActualizingMI($apiKey, \Workabox\API\ActualizingMI $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\ActualizingMIFilter $filter
     * @return \Workabox\API\ActualizingMI[]
     * @throws \Workabox\API\APIException
     */
    public function ExportActualizingMI($apiKey, $date, \Workabox\API\ActualizingMIFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\CustomerNaturalPerson $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportCustomerNaturalPerson($apiKey, \Workabox\API\CustomerNaturalPerson $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\CustomerNaturalPersonFilter $filter
     * @return \Workabox\API\CustomerNaturalPerson[]
     * @throws \Workabox\API\APIException
     */
    public function ExportCustomerNaturalPerson($apiKey, $date, \Workabox\API\CustomerNaturalPersonFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\GoodsIncome $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportGoodsIncome($apiKey, \Workabox\API\GoodsIncome $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\GoodsIncomeFilter $filter
     * @return \Workabox\API\GoodsIncome[]
     * @throws \Workabox\API\APIException
     */
    public function ExportGoodsIncome($apiKey, $date, \Workabox\API\GoodsIncomeFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\GoodsMove $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportGoodsMove($apiKey, \Workabox\API\GoodsMove $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\GoodsMoveFilter $filter
     * @return \Workabox\API\GoodsMove[]
     * @throws \Workabox\API\APIException
     */
    public function ExportGoodsMove($apiKey, $date, \Workabox\API\GoodsMoveFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\GoodsReturn $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportGoodsReturn($apiKey, \Workabox\API\GoodsReturn $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\GoodsReturnFilter $filter
     * @return \Workabox\API\GoodsReturn[]
     * @throws \Workabox\API\APIException
     */
    public function ExportGoodsReturn($apiKey, $date, \Workabox\API\GoodsReturnFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\GoodsDebit $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportGoodsDebit($apiKey, \Workabox\API\GoodsDebit $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\GoodsDebitFilter $filter
     * @return \Workabox\API\GoodsDebit[]
     * @throws \Workabox\API\APIException
     */
    public function ExportGoodsDebit($apiKey, $date, \Workabox\API\GoodsDebitFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\RetailSale $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportRetailSale($apiKey, \Workabox\API\RetailSale $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\RetailSaleFilter $filter
     * @return \Workabox\API\RetailSale[]
     * @throws \Workabox\API\APIException
     */
    public function ExportRetailSale($apiKey, $date, \Workabox\API\RetailSaleFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\CustomerOrder $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportCustomerOrder($apiKey, \Workabox\API\CustomerOrder $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\CustomerOrderFilter $filter
     * @return \Workabox\API\CustomerOrder[]
     * @throws \Workabox\API\APIException
     */
    public function ExportCustomerOrder($apiKey, $date, \Workabox\API\CustomerOrderFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\CustomerReturn $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportCustomerReturn($apiKey, \Workabox\API\CustomerReturn $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\CustomerReturnFilter $filter
     * @return \Workabox\API\CustomerReturn[]
     * @throws \Workabox\API\APIException
     */
    public function ExportCustomerReturn($apiKey, $date, \Workabox\API\CustomerReturnFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\OrderDocOut $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportOrderDocOut($apiKey, \Workabox\API\OrderDocOut $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\OrderDocOutFilter $filter
     * @return \Workabox\API\OrderDocOut[]
     * @throws \Workabox\API\APIException
     */
    public function ExportOrderDocOut($apiKey, $date, \Workabox\API\OrderDocOutFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\OrderDocInner $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportOrderDocInner($apiKey, \Workabox\API\OrderDocInner $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\OrderDocInnerFilter $filter
     * @return \Workabox\API\OrderDocInner[]
     * @throws \Workabox\API\APIException
     */
    public function ExportOrderDocInner($apiKey, $date, \Workabox\API\OrderDocInnerFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\Inventory $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportInventory($apiKey, \Workabox\API\Inventory $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\InventoryFilter $filter
     * @return \Workabox\API\Inventory[]
     * @throws \Workabox\API\APIException
     */
    public function ExportInventory($apiKey, $date, \Workabox\API\InventoryFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\MoneyMovementIn $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportMoneyMovementIn($apiKey, \Workabox\API\MoneyMovementIn $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\MoneyMovementInFilter $filter
     * @return \Workabox\API\MoneyMovementIn[]
     * @throws \Workabox\API\APIException
     */
    public function ExportMoneyMovementIn($apiKey, $date, \Workabox\API\MoneyMovementInFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\MoneyMovementOut $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportMoneyMovementOut($apiKey, \Workabox\API\MoneyMovementOut $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\MoneyMovementOutFilter $filter
     * @return \Workabox\API\MoneyMovementOut[]
     * @throws \Workabox\API\APIException
     */
    public function ExportMoneyMovementOut($apiKey, $date, \Workabox\API\MoneyMovementOutFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\MoneyMovementInner $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportMoneyMovementInner($apiKey, \Workabox\API\MoneyMovementInner $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\MoneyMovementInnerFilter $filter
     * @return \Workabox\API\MoneyMovementInner[]
     * @throws \Workabox\API\APIException
     */
    public function ExportMoneyMovementInner($apiKey, $date, \Workabox\API\MoneyMovementInnerFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\Routings $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportRoutings($apiKey, \Workabox\API\Routings $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\RoutingsFilter $filter
     * @return \Workabox\API\Routings[]
     * @throws \Workabox\API\APIException
     */
    public function ExportRoutings($apiKey, $date, \Workabox\API\RoutingsFilter $filter);
    /**
     * @param string $apiKey
     * @param \Workabox\API\ProductionOperations $importdata
     * @return int
     * @throws \Workabox\API\APIException
     */
    public function ImportProductionOperations($apiKey, \Workabox\API\ProductionOperations $importdata);
    /**
     * @param string $apiKey
     * @param string $date
     * @param \Workabox\API\ProductionOperationsFilter $filter
     * @return \Workabox\API\ProductionOperations[]
     * @throws \Workabox\API\APIException
     */
    public function ExportProductionOperations($apiKey, $date, \Workabox\API\ProductionOperationsFilter $filter);
}
