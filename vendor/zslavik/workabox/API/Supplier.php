<?php
namespace Workabox\API;

/**
 * Autogenerated by Thrift Compiler (0.12.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
use Thrift\Base\TBase;
use Thrift\Type\TType;
use Thrift\Type\TMessageType;
use Thrift\Exception\TException;
use Thrift\Exception\TProtocolException;
use Thrift\Protocol\TProtocol;
use Thrift\Protocol\TBinaryProtocolAccelerated;
use Thrift\Exception\TApplicationException;

class Supplier
{
    static public $isValidate = false;

    static public $_TSPEC = array(
        1 => array(
            'var' => 'ID',
            'isRequired' => false,
            'type' => TType::I32,
        ),
        2 => array(
            'var' => 'OuterID',
            'isRequired' => false,
            'type' => TType::STRING,
        ),
        3 => array(
            'var' => 'ObjectName',
            'isRequired' => false,
            'type' => TType::STRING,
        ),
        4 => array(
            'var' => 'Settlement',
            'isRequired' => false,
            'type' => TType::STRING,
        ),
        5 => array(
            'var' => 'PostAddress',
            'isRequired' => false,
            'type' => TType::STRING,
        ),
        6 => array(
            'var' => 'Phone',
            'isRequired' => false,
            'type' => TType::STRING,
        ),
        7 => array(
            'var' => 'ContactPerson',
            'isRequired' => false,
            'type' => TType::STRING,
        ),
        8 => array(
            'var' => 'Bank',
            'isRequired' => false,
            'type' => TType::STRUCT,
            'class' => '\Workabox\API\ObjectID',
        ),
        9 => array(
            'var' => 'AccountNumber',
            'isRequired' => false,
            'type' => TType::STRING,
        ),
        10 => array(
            'var' => 'ObjectOwner',
            'isRequired' => false,
            'type' => TType::STRUCT,
            'class' => '\Workabox\API\ObjectID',
        ),
        11 => array(
            'var' => 'OuterCodes',
            'isRequired' => false,
            'type' => TType::LST,
            'etype' => TType::STRUCT,
            'elem' => array(
                'type' => TType::STRUCT,
                'class' => '\Workabox\API\OuterCodesLine',
                ),
        ),
        12 => array(
            'var' => 'StateMain',
            'isRequired' => false,
            'type' => TType::STRING,
        ),
    );

    /**
     * @var int
     */
    public $ID = null;
    /**
     * @var string
     */
    public $OuterID = null;
    /**
     * @var string
     */
    public $ObjectName = null;
    /**
     * @var string
     */
    public $Settlement = null;
    /**
     * @var string
     */
    public $PostAddress = null;
    /**
     * @var string
     */
    public $Phone = null;
    /**
     * @var string
     */
    public $ContactPerson = null;
    /**
     * @var \Workabox\API\ObjectID
     */
    public $Bank = null;
    /**
     * @var string
     */
    public $AccountNumber = null;
    /**
     * @var \Workabox\API\ObjectID
     */
    public $ObjectOwner = null;
    /**
     * @var \Workabox\API\OuterCodesLine[]
     */
    public $OuterCodes = null;
    /**
     * @var string
     */
    public $StateMain = null;

    public function __construct($vals = null)
    {
        if (is_array($vals)) {
            if (isset($vals['ID'])) {
                $this->ID = $vals['ID'];
            }
            if (isset($vals['OuterID'])) {
                $this->OuterID = $vals['OuterID'];
            }
            if (isset($vals['ObjectName'])) {
                $this->ObjectName = $vals['ObjectName'];
            }
            if (isset($vals['Settlement'])) {
                $this->Settlement = $vals['Settlement'];
            }
            if (isset($vals['PostAddress'])) {
                $this->PostAddress = $vals['PostAddress'];
            }
            if (isset($vals['Phone'])) {
                $this->Phone = $vals['Phone'];
            }
            if (isset($vals['ContactPerson'])) {
                $this->ContactPerson = $vals['ContactPerson'];
            }
            if (isset($vals['Bank'])) {
                $this->Bank = $vals['Bank'];
            }
            if (isset($vals['AccountNumber'])) {
                $this->AccountNumber = $vals['AccountNumber'];
            }
            if (isset($vals['ObjectOwner'])) {
                $this->ObjectOwner = $vals['ObjectOwner'];
            }
            if (isset($vals['OuterCodes'])) {
                $this->OuterCodes = $vals['OuterCodes'];
            }
            if (isset($vals['StateMain'])) {
                $this->StateMain = $vals['StateMain'];
            }
        }
    }

    public function getName()
    {
        return 'Supplier';
    }


    public function read($input)
    {
        $xfer = 0;
        $fname = null;
        $ftype = 0;
        $fid = 0;
        $xfer += $input->readStructBegin($fname);
        while (true) {
            $xfer += $input->readFieldBegin($fname, $ftype, $fid);
            if ($ftype == TType::STOP) {
                break;
            }
            switch ($fid) {
                case 1:
                    if ($ftype == TType::I32) {
                        $xfer += $input->readI32($this->ID);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 2:
                    if ($ftype == TType::STRING) {
                        $xfer += $input->readString($this->OuterID);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 3:
                    if ($ftype == TType::STRING) {
                        $xfer += $input->readString($this->ObjectName);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 4:
                    if ($ftype == TType::STRING) {
                        $xfer += $input->readString($this->Settlement);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 5:
                    if ($ftype == TType::STRING) {
                        $xfer += $input->readString($this->PostAddress);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 6:
                    if ($ftype == TType::STRING) {
                        $xfer += $input->readString($this->Phone);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 7:
                    if ($ftype == TType::STRING) {
                        $xfer += $input->readString($this->ContactPerson);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 8:
                    if ($ftype == TType::STRUCT) {
                        $this->Bank = new \Workabox\API\ObjectID();
                        $xfer += $this->Bank->read($input);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 9:
                    if ($ftype == TType::STRING) {
                        $xfer += $input->readString($this->AccountNumber);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 10:
                    if ($ftype == TType::STRUCT) {
                        $this->ObjectOwner = new \Workabox\API\ObjectID();
                        $xfer += $this->ObjectOwner->read($input);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 11:
                    if ($ftype == TType::LST) {
                        $this->OuterCodes = array();
                        $_size301 = 0;
                        $_etype304 = 0;
                        $xfer += $input->readListBegin($_etype304, $_size301);
                        for ($_i305 = 0; $_i305 < $_size301; ++$_i305) {
                            $elem306 = null;
                            $elem306 = new \Workabox\API\OuterCodesLine();
                            $xfer += $elem306->read($input);
                            $this->OuterCodes []= $elem306;
                        }
                        $xfer += $input->readListEnd();
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 12:
                    if ($ftype == TType::STRING) {
                        $xfer += $input->readString($this->StateMain);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                default:
                    $xfer += $input->skip($ftype);
                    break;
            }
            $xfer += $input->readFieldEnd();
        }
        $xfer += $input->readStructEnd();
        return $xfer;
    }

    public function write($output)
    {
        $xfer = 0;
        $xfer += $output->writeStructBegin('Supplier');
        if ($this->ID !== null) {
            $xfer += $output->writeFieldBegin('ID', TType::I32, 1);
            $xfer += $output->writeI32($this->ID);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->OuterID !== null) {
            $xfer += $output->writeFieldBegin('OuterID', TType::STRING, 2);
            $xfer += $output->writeString($this->OuterID);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->ObjectName !== null) {
            $xfer += $output->writeFieldBegin('ObjectName', TType::STRING, 3);
            $xfer += $output->writeString($this->ObjectName);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->Settlement !== null) {
            $xfer += $output->writeFieldBegin('Settlement', TType::STRING, 4);
            $xfer += $output->writeString($this->Settlement);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->PostAddress !== null) {
            $xfer += $output->writeFieldBegin('PostAddress', TType::STRING, 5);
            $xfer += $output->writeString($this->PostAddress);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->Phone !== null) {
            $xfer += $output->writeFieldBegin('Phone', TType::STRING, 6);
            $xfer += $output->writeString($this->Phone);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->ContactPerson !== null) {
            $xfer += $output->writeFieldBegin('ContactPerson', TType::STRING, 7);
            $xfer += $output->writeString($this->ContactPerson);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->Bank !== null) {
            if (!is_object($this->Bank)) {
                throw new TProtocolException('Bad type in structure.', TProtocolException::INVALID_DATA);
            }
            $xfer += $output->writeFieldBegin('Bank', TType::STRUCT, 8);
            $xfer += $this->Bank->write($output);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->AccountNumber !== null) {
            $xfer += $output->writeFieldBegin('AccountNumber', TType::STRING, 9);
            $xfer += $output->writeString($this->AccountNumber);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->ObjectOwner !== null) {
            if (!is_object($this->ObjectOwner)) {
                throw new TProtocolException('Bad type in structure.', TProtocolException::INVALID_DATA);
            }
            $xfer += $output->writeFieldBegin('ObjectOwner', TType::STRUCT, 10);
            $xfer += $this->ObjectOwner->write($output);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->OuterCodes !== null) {
            if (!is_array($this->OuterCodes)) {
                throw new TProtocolException('Bad type in structure.', TProtocolException::INVALID_DATA);
            }
            $xfer += $output->writeFieldBegin('OuterCodes', TType::LST, 11);
            $output->writeListBegin(TType::STRUCT, count($this->OuterCodes));
            foreach ($this->OuterCodes as $iter307) {
                $xfer += $iter307->write($output);
            }
            $output->writeListEnd();
            $xfer += $output->writeFieldEnd();
        }
        if ($this->StateMain !== null) {
            $xfer += $output->writeFieldBegin('StateMain', TType::STRING, 12);
            $xfer += $output->writeString($this->StateMain);
            $xfer += $output->writeFieldEnd();
        }
        $xfer += $output->writeFieldStop();
        $xfer += $output->writeStructEnd();
        return $xfer;
    }
}
