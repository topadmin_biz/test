<?php
namespace Workabox\API;

/**
 * Autogenerated by Thrift Compiler (0.12.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
use Thrift\Base\TBase;
use Thrift\Type\TType;
use Thrift\Type\TMessageType;
use Thrift\Exception\TException;
use Thrift\Exception\TProtocolException;
use Thrift\Protocol\TProtocol;
use Thrift\Protocol\TBinaryProtocolAccelerated;
use Thrift\Exception\TApplicationException;

class CashDeskFilter
{
    static public $isValidate = false;

    static public $_TSPEC = array(
        1 => array(
            'var' => 'ID',
            'isRequired' => false,
            'type' => TType::I32,
        ),
        2 => array(
            'var' => 'OuterID',
            'isRequired' => false,
            'type' => TType::STRING,
        ),
        3 => array(
            'var' => 'ObjectName',
            'isRequired' => false,
            'type' => TType::STRING,
        ),
        4 => array(
            'var' => 'ObjectOwner',
            'isRequired' => false,
            'type' => TType::STRUCT,
            'class' => '\Workabox\API\ObjectID',
        ),
        5 => array(
            'var' => 'HierarchyBranch',
            'isRequired' => false,
            'type' => TType::STRING,
        ),
        6 => array(
            'var' => 'StateMain',
            'isRequired' => false,
            'type' => TType::LST,
            'etype' => TType::STRING,
            'elem' => array(
                'type' => TType::STRING,
                ),
        ),
        7 => array(
            'var' => 'CashDeskOfficeMaster',
            'isRequired' => false,
            'type' => TType::STRUCT,
            'class' => '\Workabox\API\ObjectID',
        ),
    );

    /**
     * @var int
     */
    public $ID = null;
    /**
     * @var string
     */
    public $OuterID = null;
    /**
     * @var string
     */
    public $ObjectName = null;
    /**
     * @var \Workabox\API\ObjectID
     */
    public $ObjectOwner = null;
    /**
     * @var string
     */
    public $HierarchyBranch = null;
    /**
     * @var string[]
     */
    public $StateMain = null;
    /**
     * @var \Workabox\API\ObjectID
     */
    public $CashDeskOfficeMaster = null;

    public function __construct($vals = null)
    {
        if (is_array($vals)) {
            if (isset($vals['ID'])) {
                $this->ID = $vals['ID'];
            }
            if (isset($vals['OuterID'])) {
                $this->OuterID = $vals['OuterID'];
            }
            if (isset($vals['ObjectName'])) {
                $this->ObjectName = $vals['ObjectName'];
            }
            if (isset($vals['ObjectOwner'])) {
                $this->ObjectOwner = $vals['ObjectOwner'];
            }
            if (isset($vals['HierarchyBranch'])) {
                $this->HierarchyBranch = $vals['HierarchyBranch'];
            }
            if (isset($vals['StateMain'])) {
                $this->StateMain = $vals['StateMain'];
            }
            if (isset($vals['CashDeskOfficeMaster'])) {
                $this->CashDeskOfficeMaster = $vals['CashDeskOfficeMaster'];
            }
        }
    }

    public function getName()
    {
        return 'CashDeskFilter';
    }


    public function read($input)
    {
        $xfer = 0;
        $fname = null;
        $ftype = 0;
        $fid = 0;
        $xfer += $input->readStructBegin($fname);
        while (true) {
            $xfer += $input->readFieldBegin($fname, $ftype, $fid);
            if ($ftype == TType::STOP) {
                break;
            }
            switch ($fid) {
                case 1:
                    if ($ftype == TType::I32) {
                        $xfer += $input->readI32($this->ID);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 2:
                    if ($ftype == TType::STRING) {
                        $xfer += $input->readString($this->OuterID);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 3:
                    if ($ftype == TType::STRING) {
                        $xfer += $input->readString($this->ObjectName);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 4:
                    if ($ftype == TType::STRUCT) {
                        $this->ObjectOwner = new \Workabox\API\ObjectID();
                        $xfer += $this->ObjectOwner->read($input);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 5:
                    if ($ftype == TType::STRING) {
                        $xfer += $input->readString($this->HierarchyBranch);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 6:
                    if ($ftype == TType::LST) {
                        $this->StateMain = array();
                        $_size497 = 0;
                        $_etype500 = 0;
                        $xfer += $input->readListBegin($_etype500, $_size497);
                        for ($_i501 = 0; $_i501 < $_size497; ++$_i501) {
                            $elem502 = null;
                            $xfer += $input->readString($elem502);
                            $this->StateMain []= $elem502;
                        }
                        $xfer += $input->readListEnd();
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 7:
                    if ($ftype == TType::STRUCT) {
                        $this->CashDeskOfficeMaster = new \Workabox\API\ObjectID();
                        $xfer += $this->CashDeskOfficeMaster->read($input);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                default:
                    $xfer += $input->skip($ftype);
                    break;
            }
            $xfer += $input->readFieldEnd();
        }
        $xfer += $input->readStructEnd();
        return $xfer;
    }

    public function write($output)
    {
        $xfer = 0;
        $xfer += $output->writeStructBegin('CashDeskFilter');
        if ($this->ID !== null) {
            $xfer += $output->writeFieldBegin('ID', TType::I32, 1);
            $xfer += $output->writeI32($this->ID);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->OuterID !== null) {
            $xfer += $output->writeFieldBegin('OuterID', TType::STRING, 2);
            $xfer += $output->writeString($this->OuterID);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->ObjectName !== null) {
            $xfer += $output->writeFieldBegin('ObjectName', TType::STRING, 3);
            $xfer += $output->writeString($this->ObjectName);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->ObjectOwner !== null) {
            if (!is_object($this->ObjectOwner)) {
                throw new TProtocolException('Bad type in structure.', TProtocolException::INVALID_DATA);
            }
            $xfer += $output->writeFieldBegin('ObjectOwner', TType::STRUCT, 4);
            $xfer += $this->ObjectOwner->write($output);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->HierarchyBranch !== null) {
            $xfer += $output->writeFieldBegin('HierarchyBranch', TType::STRING, 5);
            $xfer += $output->writeString($this->HierarchyBranch);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->StateMain !== null) {
            if (!is_array($this->StateMain)) {
                throw new TProtocolException('Bad type in structure.', TProtocolException::INVALID_DATA);
            }
            $xfer += $output->writeFieldBegin('StateMain', TType::LST, 6);
            $output->writeListBegin(TType::STRING, count($this->StateMain));
            foreach ($this->StateMain as $iter503) {
                $xfer += $output->writeString($iter503);
            }
            $output->writeListEnd();
            $xfer += $output->writeFieldEnd();
        }
        if ($this->CashDeskOfficeMaster !== null) {
            if (!is_object($this->CashDeskOfficeMaster)) {
                throw new TProtocolException('Bad type in structure.', TProtocolException::INVALID_DATA);
            }
            $xfer += $output->writeFieldBegin('CashDeskOfficeMaster', TType::STRUCT, 7);
            $xfer += $this->CashDeskOfficeMaster->write($output);
            $xfer += $output->writeFieldEnd();
        }
        $xfer += $output->writeFieldStop();
        $xfer += $output->writeStructEnd();
        return $xfer;
    }
}
